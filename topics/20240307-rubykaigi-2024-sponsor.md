---
layout: default
main_title: クリアコードはRubyKaigi 2024のシルバースポンサーになりました。
sub_title:
type: topic
---

クリアコードは、2024年5月15日（水）～17日（金）に開催される、RubyKaigi 2024を[シルバースポンサー](https://rubykaigi.org/2024/sponsors/#sponsor-570)として支援します。


![]({% link images/RubyKaigi_2024.png %})
[^1]

RubyKaigiは、国内最大規模で開催されるRubyの国際カンファレンスです。期間中は、関連のイベントなども多く開催され多くのRubyistたちが交流を深める場となっています。クリアコードは、2009年の日本Ruby会議2009からRubyKaigiをスポンサーしています。

沖縄県那覇市でオフライン開催されるRubyKaigi 2024には、Rubyコミッターの須藤含めクリアコードから数人参加予定です。

## RubyKaigi 2024 概要

* 日時: 2024年5月15日（水）～17日（金）
* 会場:  那覇文化芸術劇場なはーと 
* ウェブサイト：https://rubykaigi.org/2024/
  ウェブサイトから参加申し込みが可能です。

[^1]:RubyKaigi 2024 logo. This work by RubyKaigi 2024 Team is licensed under a Creative Commons Attribution 4.0 Unported License.
