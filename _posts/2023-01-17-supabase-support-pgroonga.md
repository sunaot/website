---
title: SupabaseでマネージドなPGroongaを使える！
author: komainu8
tags:
  - groonga
---

[PGroonga](https://pgroonga.github.io/ja/)の[サポートサービス]({% link services/groonga.md %})を担当している堀本です。

PGroongaはPostgreSQLで高速に全文検索するための拡張ですが、 Amazon RDS や Azure Database for PostgreSQL などのマネージドなPostgreSQLでは使えませんでしたが
2022-12-16 から[Supabase](https://supabase.com/) がPGroongaをサポートしました。

これによって、マネージドなPGroongaを使うことができます！

<!--more-->

### Supabaseとは

Supabaseは、Firebaseに代わるオープンソースのサービスです。PostgreSQLデータベース、認証システム、API機能、エッジファンクション、リアルタイムサブスクリプションやストレージなど、開発者がプロダクトを作るのに必要なバックエンドの全ての機能を提供します。PosgresSQLはSupabaseの中核をなしており、PGroongaを含めて40以上の拡張機能とともにネイティブに動作しています。

マネージドなPostgreSQLでPGroongaを使いたいというリクエストはよくいただくのですが、これまでは実現できていませんでした。
しかしながら、今回、SupabaseでPGroongaが使えるようになったので、PGroongaを使ってみたいけれど、インストール作業や日々のメンテンナンスなどの
運用面のコストが高く導入をためらっていた方は、PGroongaを試すチャンスです！
Supabaseには、無料のサービスプランもあります。
Supabaseの詳細や無料のサービスプラン以外のプランについては、 [Supabaseのウェブサイト](https://supabase.com/pricing) を参照してください。

### メンテナンスへのサポート

SupabaseがPGroongaを採用する際にSupabaseのCEOの方から連絡をいただき、PGroongaを採用するにあたりサポートできることがあるか？とご提案いただきました。
日本の企業から問い合わせがある場合、その企業には具体的な課題があります。そのため、技術サポート・コンサルティングサポートを提案してその課題の解決に対して対価をいただくという契約をしています。しかし、今回の場合は（少なくとも現時点では）具体的な課題はありませんでした。

Supabaseはサブスクリプション型のサービスです。サブスクリプション型のサービスは継続的に安定利用できることが重要です。そのため、私たちの普段のPGroongaのメンテナンスに対して定期的に一定額を支払ってくれないか？と提案しました。PGroongaが継続的にメンテナンスされることはSupabaseにとっても価値があることです。Supabaseが有効にしている拡張がメンテナンスされていないとSupabaseで安心して利用できないからです。この提案が通ったので普段のメンテンナンスについて対価をいただくことになりました。なお、この協力関係には普段のメンテナンスだけでなくPGroongaユーザーにSupabaseを紹介することも含めています。PGroongaユーザーが増えることにつながるのでSupabaseにとってもPGroongaにとってもうれしいことだからです。PGroonga開発チームと同様の協力関係を築きたい企業は[お問い合わせ]({% link contact/index.md %})ください。

普段のメンテンナンスとは、PGroongaを新しいPostgreSQLに対応することや、コミュニティからの問題報告、質問への対応です。これらの活動は目立ちませんがそれなりのコストがかかります。
日々のメンテナンスをおろそかにすると、迅速にリリースできなかったり、リリース後に問題が見つかったりするなどの問題につながります。
これらの問題が発生するのは、PGroongaのユーザーも困ります。したがって、日々のメンテナンスに対してお金を支払ってメンテナンスを継続しやすくすることは、PGroongaのユーザーには安定利用できるメリットがありますし、PGroongaの開発者には継続して対価をいただけるというメリットがあります。

また、海外の企業、特にアメリカの企業は自社が利用しているフリーソフトウェアに対して費用支援することに前向きです。
これは、ソフトウェアの開発者、利用者、提供者、エンドユーザーがお互いに利益を共有し、ソフトウェアを活性化させ継続させていくという思想が浸透していることが一因のように感じられます。

### まとめ

フリーソフトウェアはソースコードを自由に研究し改変する自由があるので、メンテンナンスすることに特別の制限はありませんが、継続してメンテンナンスするにはプロダクトへの理解が必要で一朝一夕でできるようになるものではありません。
メンテナンスの継続はソフトウェアを使用するにあたって必要なコストなので、この事例のようにメンテンナンスの継続をサポートしたい方は、是非[ご相談]({% link contact/index.md %})ください。
この事例では [Open Collective](https://opencollective.com/pgroonga) という仕組みを使ってサポート頂いていますが Open Collective 以外の GitHub Sponsors などの仕組みでも対応可能です。
