---
tags: []
title: Debianパッケージのメンテナンスをsalsa.debian.orgに移行後にアップデートするには
---
### はじめに

以前、[Debianパッケージのメンテナンスをsalsa.debian.orgに移行するには]({% post_url 2018-03-24-index %})というタイトルで、パッケージのメンテナンスをsalsa.debian.orgに移行するのにともない、git-buildpackage(以降gbpと記載)のやりかたに合わせつつリリースするところまでの内容を記事にまとめました。
<!--more-->


今回は、アップストリームで新しいバージョンがリリースされたときに、Debianパッケージをgbpを使って更新するときのやりかたを紹介します。

### 前提条件

gbpを使って作業する際、環境が違っているとそのままではうまくいかないことがあります。
そのため、本記事では[Debianパッケージのメンテナンスをsalsa.debian.orgに移行するには]({% post_url 2018-03-24-index %})で紹介した手順で環境をセットアップ済みであることを前提とします。

  * 前回記事で紹介した~/.gbp.confを設定している

  * ブランチはmasterではなくdebian/unstableにしている

  * ビルド方法にはpbuilder+tmpfsを利用する構成になっている

Groonga 8.0.1 から Groonga 8.0.2 にDebianパッケージを更新するのにどのようにしたかを説明します。

### リポジトリをcloneする

作業用のリポジトリをcloneします。この操作にはsalsa.d.oにアカウントが必要です。https経由（https://salsa.debian.org/debian/groonga.git ）ならsalsa.d.oにアカウントがなくても大丈夫です。

```
% git clone git@salsa.debian.org:debian/groonga.git
```


### アップストリームのソースをインポートする

Groongaの場合、 `debian/watch` があるので以下のコマンドを実行することで、ソースをインポートすることができました。

```
% cd groonga
% gbp import-orig --uscan --debian-branch=debian/unstable
gbp:info: Launching uscan...
uscan: Newest version of groonga on remote site is 8.0.2, local version is 8.0.1
uscan:    => Newer package available from
      https://packages.groonga.org/source/groonga/groonga-8.0.2.tar.gz
gpgv: 2018年04月27日 17時47分26秒 JSTに施された署名
gpgv:                DSA鍵C97E4649A2051D0CEA1A73F972A7496B45499429を使用
gpgv: "groonga Key (groonga Official Signing Key) <packages@groonga.org>"からの正しい署名
gbp:info: Using uscan downloaded tarball ../groonga_8.0.2.orig.tar.gz
What is the upstream version? [8.0.2] 
gbp:info: Importing '../groonga_8.0.2.orig.gbp.tar.gz' to branch 'upstream' (filtering out ['*egg.info', '.bzr', '.hg', '.hgtags', '.svn', 'CVS', '*/debian/*', 'debian/*'])...
gbp:info: Source package is groonga
gbp:info: Upstream version is 8.0.2
gbp:info: Replacing upstream source on 'debian/unstable'
gbp:info: Successfully imported version 8.0.2 of ../groonga_8.0.2.orig.gbp.tar.gz
```


上記のようにすることで、 `upstream` ブランチと `pristine-tar` ブランチが更新されます。

```
% git checkout pristine-tar
Switched to branch 'pristine-tar'
Your branch is ahead of 'origin/pristine-tar' by 1 commit.
  (use "git push" to publish your local commits)
% git push
Counting objects: 4, done.
Delta compression using up to 4 threads.
Compressing objects: 100% (3/3), done.
Writing objects: 100% (4/4), 399.92 KiB | 13.79 MiB/s, done.
Total 4 (delta 1), reused 0 (delta 0)
remote:
remote: To create a merge request for pristine-tar, visit:
remote:   https://salsa.debian.org/debian/groonga/merge_requests/new?merge_request%5Bsource_branch%5D=pristine-tar
remote:
To salsa.debian.org:debian/groonga.git
   f46d803..95ccd57  pristine-tar -> pristine-tar
% git checkout upstream
Switched to branch 'upstream'
Your branch is ahead of 'origin/upstream' by 1 commit.
  (use "git push" to publish your local commits)
% git push
Total 0 (delta 0), reused 0 (delta 0)
remote:
remote: To create a merge request for upstream, visit:
remote:   https://salsa.debian.org/debian/groonga/merge_requests/new?merge_request%5Bsource_branch%5D=upstream
remote:
To salsa.debian.org:debian/groonga.git
   68022cf..8f10dc8  upstream -> upstream
```


更新されたブランチをそれぞれpushしておきます。

### debian/changelogを更新する

アップストリームの更新であることを示す変更点を `debian/changelog` に記載します。

```
groonga (8.0.2-1) unstable; urgency=medium

  * New upstream version 8.0.2.

 -- Kentaro Hayashi <hayashi@clear-code.com>  Wed, 02 May 2018 10:25:09 +0900                                                              
```


### パッケージをビルド、テストする

ここまでできたら、きちんとパッケージとしての体裁が整っているか、ビルドします。

```
% gbp buildpackage --git-ignore-new
```


ビルドの過程のdh cleanで、ソースツリーが変更されてしまう場合には、 `--git-ignore-new` を指定します。
ビルド後にはlintianによるパッケージのチェック結果が表示されるので、報告された問題を都度修正するのがよいでしょう。

次に、パッケージのインストールなどに問題がないかテストします。

パッケージのインストールテストにはpiupartsを用います。

```
% sudo piuparts -d sid  -t /var/cache/pbuilder/build -b /var/cache/pbuilder/unstable-amd64-base.tgz ../build-area/*.deb
```


紹介した `.gbp.conf` の設定をしていれば、cloneしたリポジトリと同階層に `build-area` ディレクトリが作成され、そこにdebパッケージが配置されているはずです。
piupartsによるテストをパスしたら、次はアップロードしましょう。

### mentors.d.nへソースパッケージをアップロード

mentors.d.nへアップロードするためのソースパッケージを用意します。

```
% debuild -S
```


パッケージに署名をしたら、dputで実際にmentors.d.nへとアップロードします。

```
% dput mentors ../groonga_8.0.2-1_source.changes 
```


### RFSを送りスポンサーを探す

bugs.debian.orgにスポンサーを探すメールを投げます。

Groonga 8.0.2-1の場合は次のようなbugとして登録しました。

  * [RFS: groonga/8.0.2-1](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=897393)

### gbpでタグを打つ

特に問題なければあとはunstableへとアップロードされるのを待つだけです。ただし、パッケージング内容にコメントがつくことがあります。
その場合は都度対応してソースパッケージを再びアップロードします。

パッケージの修正が追加で必要になることもあるので、タグを打つのは実際にunstableに入ってからのほうがよいかもしれません。

タグを打つには、以下のコマンドを実行します。

```
% gbp buildpackage --git-tag --git-debian-branch=debian/unstable
```


ここまでで、gbpでDebianパッケージのアップデートをリリースするところまでの作業が完了です。

### まとめ

今回は、メンテナンスしているパッケージをgbpのやりかたに合わせつつアップデートする方法を紹介しました。

必要最低限のgbpの使い方しか紹介していないので、詳細については、http://honk.sigxcpu.org/projects/git-buildpackage/manual-html/gbp.html を参照してください。
