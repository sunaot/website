---
tags:
- embedded
title: YoctoでFcitxをビルドする
---
以前の記事で、Yoctoのweston環境向けに[uim](https://github.com/uim/uim)をビルドして日本語入力を実現する方法を紹介しました。
<!--more-->


  * [YoctoのWeston上で日本語入力]({% post_url 2018-08-31-index %})

その後、別のお客様向けにYocto上での[Fcitx 4](https://gitlab.com/fcitx/fcitx)のクロスビルドを支援する機会があり、その成果を[meta-inputmethod](https://gitlab.com/clear-code/meta-inputmethod/)として公開しましたので紹介します。

### meta-inputmethodの現在のステータス

クリアコードは、仕事で書いたコードは出来る限りお客様からの許可を頂いて[フリーソフトウェア](https://www.gnu.org/philosophy/free-sw.ja.html)として公開するようにしています。今回作成したFcitxのYoctoレシピについても許可を頂いたので公開することとしました。

その際、Yoctoプロジェクトにフィードバックして、uimと同じように[poky](http://git.yoctoproject.org/cgit/cgit.cgi/poky)に入れてもらうという案もあったのですが、[Yoctoのコンセプト](https://www.yoctoproject.org/docs/2.6/overview-manual/overview-manual.html)的には別のYoctoレイヤに分けた方が管理が楽であろうと考え、[meta-inputmethod](https://gitlab.com/clear-code/meta-inputmethod/)として公開することとしました。現在、meta-inputmethodには以下のレシピが含まれています。

  * IMフレームワーク: [Fcitx 4](https://gitlab.com/fcitx/fcitx)

  * 日本語変換エンジン: Anthy

  * 上記の依存ライブラリ

X11環境ではおそらく動作するものと思われますが、実はクリアコードではX11環境での検証はまだ行っておりません。このレシピは、主に[Wayland](https://wayland.freedesktop.org/)環境でFcitxを動作させることが可能かどうかを検証するために作成したものだからです。Wayland環境では動作を検証していますが、以下の問題があることを確認しています。

  * 候補ウィンドウを表示させるにはXWaylandを有効化させる必要がある

  * 上記を行っても、候補ウィンドウを正しい位置に表示させることができない

  * Ctrl + Spaceで日本語入力のON/OFFをすることができない

Fcitx 4はもともと[Waylandには正式対応はしていない](https://www.csslayer.info/wordpress/fcitx-dev/gaps-between-wayland-and-fcitx-or-all-input-methods/)ため、これは致し方ないところです。

### ビルド方法

meta-inputmethodおよび依存レイヤであるmeta-qt5を他のYoctoレイヤと同ディレクトリにcloneします。

```console
$ git clone https://gitlab.com/clear-code/meta-inputmethod.git
$ git clone https://github.com/meta-qt5/meta-qt5.git
```


Yoctoビルドディレクトリの`conf/bblayers.conf` に以下を追記します。pathは環境に応じて調整して下さい。

```text
BBLAYERS += "
  /path/to/meta-qt5 \
  /path/to/meta-inputmethod
"
```


また、`conf/local.conf` に以下の内容を追記します。こちらも、必要の無い物は削除するなどして調整して下さい。

```text
IMAGE_INSTALL_append = " \
  fcitx \
  fcitx-data \
  fcitx-anthy \
  fcitx-gtk2.0 \
  fcitx-gtk3 \
  fcitx-qt5 \
  fcitx-ui-classic \
  fcitx-module-dbus \
  fcitx-configtool \
  fcitx-module-x11 \
"
```


`bitbake core-image-sato` などを実行することで、ブートイメージにFcitxを追加することができます。

実行時の設定は今のところレシピでは追加していませんので、手動で設定する必要があります。

例えば、`/etc/environment` 等に以下を追記し

```shell
export GTK_IM_MODULE=fcitx
export GTK3_IM_MODULE=fcitx
export XMODIFIERS="@im=fcitx"
export QT_IM_MODULE=fcitx
```


手動でFcitxデーモンを起動することで、動作を確認できるかと思います。

```console
$ fcitx
```


DBusが起動していない場合は、[dbus-launch](https://dbus.freedesktop.org/doc/dbus-launch.1.html)等で事前にセッションバスを立ち上げ、同セッション上でfcitxを起動する必要があります。

また、Waylandで使用する場合には、XWaylandを有効化していないと候補ウィンドウを表示することができません。WaylandコンポジターとしてWestonを使用する場合、以下の設定を`/etc/xdg/weston/weston.ini`に追加します。

```text
[core]
modules=xwayland.so
xwayland=true
```


### まとめ

[meta-inputmethod](https://gitlab.com/clear-code/meta-inputmethod/)を用いてYoctoでFcitxをビルドする方法を紹介しました。現在はFcitx + Anthyのみであり、動作確認も十分に出来ていない状態ではありますが、今後Yoctoでの多言語入力環境を充実させる機会があれば、同レイヤにレシピを追加させていきたいと思います。
