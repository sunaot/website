---
tags: []
title: RabbitでスライドやPDFの自動ページ送り（スライドショー）をする方法
---
この記事では、プレゼンテーションツール[Rabbit](http://rabbit-shocker.org/ja/)でスライドやPDFの自動ページ送り（スライドショー）を実現する方法を説明します。Rabbit以外で作成したPDFのスライドショーにも使うことができます。
<!--more-->


用途としては、イベントのブースでコミュニティの紹介スライドを流しっぱなしにしておく使い方などを想定しています。

主な対象読者はRabbitでスライドを作成したことがある方ですが、Rabbitを使ったことがない方でも概要を知ることができる構成になっています。

### Rabbitとは

Rabbitはプログラマー向け（主にRubyist向け）のプレゼンテーションツールです。

RDやMarkdownなどのテキスト形式でプレゼン資料を作成できるので、好きなエディターでスライドを作成できる、バージョン管理できるなどのメリットがあります。

RubyGems.orgで公開されているので、以下のコマンドでインストールできます。

```
$ gem install rabbit
```


マルチプラットフォーム対応なので、LinuxだけでなくOS XやWindowsでも動きます。各プラットフォーム向けのインストール方法は[Rabbit - インストール](http://rabbit-shocker.org/ja/install/)にまとまっています。

スライドの作成方法は[Rabbit - スライドの作り方](https://rabbit-shocker.org/ja/how-to-make/)や[Rabbit - rabbit-slideコマンドの使い方](https://rabbit-shocker.org/ja/usage/rabbit-slide.html)をご覧ください。作成されたスライドは[Rabbit Slide Show](https://slide.rabbit-shocker.org/)で公開されています。

### Rabbitのテーマ機能

スライドショーはテーマを使って実現するので、テーマについて簡単に説明します。

Rabbitにはテーマ機能があり、スライドの見た目を内容とは別に作成して管理することができます（テーマはRubyで書くことができます）。Rabbit本体にも[組み込みのテーマ](https://github.com/rabbit-shocker/rabbit/tree/master/lib/rabbit/theme)がたくさん含まれていますし、サードパーティ製のテーマはRubyGemsで公開されています。Rabbitのテーマは`rabbit-theme-`で始まっているので、[簡単に検索できます](https://rubygems.org/search?utf8=%E2%9C%93&query=rabbit-theme)。

以下はサードパーティ製テーマの例です。

  * [rabbit-shocker/rabbit-theme-sprk2012: Theme for Sapporo RubyKaigi 2012](https://github.com/rabbit-shocker/rabbit-theme-sprk2012)

  * [groonga/rabbit-theme-groonga: A Rabbit theme for Groonga Project](https://github.com/groonga/rabbit-theme-groonga)

  * [rurema/rabbit-theme-rurema: Rabbit theme for rurema](https://github.com/rurema/rabbit-theme-rurema)

テーマの中には組み合わせて使うことを想定されているテーマもあります。たとえば、`lightning-talk-toolkit`というテーマを組み合わせることで、高橋メソッドのような大きな文字を実現することができます。

### スライドショーのやり方

スライドショーは`slide-show`テーマを使って実現することができます。

やり方は3通りあります。

  * （A）右クリックメニューからテーマを追加する

  * （B）起動時のオプションにテーマを指定する

  * （C）専用のテーマを作成する

（A）は簡単ですが、カスタマイズができません。（B）は事前にPDFを生成しておく必要がありますが、Rabbit以外で作成したPDFも使えます。（C）は少し手間がかかりますが、間隔やループの有無のカスタマイズができます。

#### （A）右クリックメニューからテーマを追加する

右クリックメニューからテーマを追加して、途中からスライドショーを開始するやり方です。

このやり方は簡単です。Rabbitを起動したあとに、右クリックメニュー（実はスライドショー以外にもいろいろなことができます）から「テーマを追加」＞「時間」＞「スライドショー」を選択するだけです。そうすれば自動で次のページに進むようになります。次のページに進むまでの間隔は、「持ち時間（allotted time）／スライドの全ページ数」になります（持ち時間が設定されていない場合は60秒です）。

#### （B）起動時のオプションにテーマを指定する

起動時のオプションにテーマを指定して、最初からスライドショーさせるやり方です。

コマンドラインでRabbitを起動するときに、`--theme`オプションに`slide-show`を指定します。このやり方はテーマが上書きされてしまうので、事前にPDFを生成してから使います。PDFは以下のコマンドで生成できます。

```
$ rabbit --print --output-filename=XXX.pdf XXX.rd
```


オプションを短縮して以下のように書くこともできます（詳細は`--help`オプションで見られます）。

```
$ rabbit -p -o XXX.pdf XXX.rd
```


そして以下のコマンドを実行すれば、最初からスライドショーする状態でRabbitを起動できます。

```
$ rabbit --theme=slide-show XXX.pdf
```


また、PDFの再生時に持ち時間を指定する場合は`--allotted-time`オプションを指定します。Rabbit以外で作成したPDFにかめとうさぎのタイマーを表示するときによく使われるやり方です。

```
$ rabbit --allotted-time=1m XXX.pdf
```


`--theme=slide-show`オプションと`--allotted-time`オプションを組み合わせることで、スライドショーの間隔を調整することができます。

```
$ rabbit --theme=slide-show --allotted-time=1m XXX.pdf
```


#### （C）専用のテーマを作成する

スライドショー専用のテーマを作成するやり方です。少し手間がかかりますが、間隔やループの有無のカスタマイズができます。

`rabbit-theme`コマンドを使って本格的にテーマを作成してもよいのですが、ここではカレントディレクトリーのテーマファイルを使う簡易的なやり方について説明します。

用意するものは、以下の2ファイルです。

  * 内容を書くファイル

  * テーマを書くファイル

##### 内容を書くファイル

ファイル名は自由ですが、タイトルの英語を付けることが多いです。拡張子はファイル形式に合わせます。

以下はサンプルです。RD形式なので、ファイル名はshiritori.rdとします。

```rd
= しりとり

: theme
   .

= 1ページ目

りす

= 2ページ目

すいか

= 3ページ目

かかし
```


タイトルページのメタ情報のthemeに、テーマ名ではなく`.`（ドット）を指定するのがポイントです。

Markdown形式の場合は以下のようになります。

```md
# しりとり

theme
:    .

# 1ページ目

りす

# 2ページ目

すいか

# 3ページ目

かかし
```


##### テーマを書くファイル

ファイル名は`theme.rb`とします。サンプルは以下です。

```ruby
include_theme("clear-blue")

@slide_show_span = 1000  # ミリ秒
@slide_show_loop = true  # true/false

include_theme("slide-show")
```


`include_theme`には好きなテーマを設定します。ここでは、Rabbit本体に組み込みで入っていて人気のある`clear-blue`テーマを使います。

`@slide_show_span`と`@slide_show_loop`で、次のページに進むまでの間隔とループするかどうかを設定することができます。`@slide_show_span`はミリ秒で指定します。`@slide_show_loop`はループするなら`true`、しないなら`false`を設定します。

最後に、再び`include_theme`を使って`slide-show`テーマを組み込みます。

##### 実行

以上の2ファイルを同じディレクトリーに置き、内容を書いたファイルを引数に指定して`rabbit`コマンドを実行します。

```
$ rabbit shiritori.rd
```


そうすると、1000ミリ秒（1秒）間隔でページが進んでループするスライドショーが始まります。

### まとめ

[Rabbit](http://rabbit-shocker.org/ja/)でスライドやPDFのスライドショーを実現する方法を説明しました。興味のある方はコミュニティーにも参加してみてください。

  * [Rabbit - Rabbitショッカー - ユーザーのみなさん](http://rabbit-shocker.org/ja/users.html)
