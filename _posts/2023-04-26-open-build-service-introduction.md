---
title: "Open Build Serviceを利用してパッケージを公開する方法"
author: kenhys
tags:
  - presentation
---

### はじめに

自由なソフトウェアを開発・配布する際、パッケージを作成し、独自にリポジトリを公開するということがあります。
導入障壁を下げ、より多くの人に使ってもらいやすくするという意味では有用です。
その反面、aptやdnfコマンドでパッケージをインストールできるようにするために、リポジトリの公開まですべて自前で頑張ろうとするのはなかなか大変です。

そこで、パッケージの作成や配布のサービスを提供している[Open Build Service](https://openbuildservice.org/)のインスタンスを利用する方法を紹介します。(本記事は、debやrpmといったパッケージをビルドしてみたことがある人を想定しています)

<!--more-->

### Open Build Serviceとは

Open Build Serviceとは、主要なディストリビューション向けにパッケージの作成や配布のサービスを提供するためのソフトウェアです。
ディストリビューションの1つである[openSUSE](https://www.opensuse.org/)の開発に利用されています。

実際のサービスは[openSUSE Build Service](https://build.opensuse.org/)として運用されています。
また、自分でOpen Build Serviceのインスタンスを立てることもできるようになっています。[^self-hosted]

[^self-hosted]: https://openbuildservice.org/download/ よりアプライアンス用途のISOイメージが提供されています。

openSUSE Build Serviceは個人向けにも開放されているので、実際に試してみたときのあれこれを[2023年4月のDebian勉強会](https://debianjp.connpass.com/event/276050/)にて発表しました。
当日の資料は[Rabbit Slide Show](https://slide.rabbit-shocker.org/)にて公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-obs-howto-202304/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-obs-howto-202304/" title="Open Build Serviceを使ってみた話">Open Build Serviceを使ってみた話</a>
  </div>
</div>

### openSUSE Build Serviceを利用するには

openSUSE Build Serviceを利用するには次の手順を踏みます。

* アカウントを作成する
* 対応するディストリビューションを決める
* プロジェクトのメタ情報を設定する
* プロジェクトのフラグ情報を設定する(必要に応じて)
* パッケージのメタ情報を設定する
* ソースパッケージをコミットする

ここまでできたらあとはパッケージのインストール手順を案内すればよいです。
実際に利用できるようにする方法は後述します。

#### アカウントを作成する

[Sign Up](https://idp-portal.suse.com/univention/self-service/#page=createaccount)よりアカウントを作成します。
ここで作成したアカウントは[openSUSE Management Tool](https://progress.opensuse.org/)といったopenSUSEの他のサービスでも利用できます。

アカウントを作成したら、`home:(ユーザー名)`というプロジェクトを利用できるようになります。

#### oscコマンドを利用できるようにする

openSUSE Build Serviceをコマンドラインから操作するために`osc`というツールがあります。
Debian系では、同名のパッケージとしてインストールできます。

```console
$ sudo apt install -y osc
```

インストールできたら、次のようにしてプロジェクトをチェックアウトしてみましょう。 

```console
$ osc checkout home:(ユーザー名)
```

初回のチェックアウトでアカウント情報を入力することになります。

`osc`の設定は`~/.config/osc/oscrc`に保存されます。

#### 対応するディストリビューションを設定する

この段階では、まだどのディストリビューション向けのプロジェクトにするかを設定できていません。

チェックアウトしたプロジェクトのディレクトリに移動たら、次のコマンドを実行してそのための情報を設定します。

```console
$ osc meta prj home:(ユーザー名)
```

エディタが起動するので、次のようなメタ情報を設定します。

```xml
<project name="home:(ユーザー名)">
<title/>
<description/>
<person userid="(ユーザー名)" role="maintainer"/>
<repository name="bullseye">
  <path project="Debian:11" repository="standard"/>
  <path project="Debian:11" repository="update"/>
  <arch>x86_64</arch>
  <arch>i586</arch>
  <arch>aarch64</arch>
</repository>
</project>
```

`<repository>`タグの`name`属性でリポジトリ名を指定します。ここで指定した名前が、後述するリポジトリの公開の際に使われます。

`<path>`でどのディストリビューションのリポジトリを参照するかを指定します。
対応アーキテクチャは`<arch>`で指定します。[^dists-and-repositories]

[^dists-and-repositories]: どんなディストリビューションに対応しているかは、`osc dists`コマンドを実行すると得られます。
`osc repositories Debian:11`を実行すると、`<path>`や`<arch>`に指定できる内容がわかります。

#### プロジェクトのフラグ情報を設定する(必要に応じて)

AlmaLinux:8などのようにAppStreamのモジュールとして各種パッケージが提供されている場合(`dnf module list`で対象を確認できる)、既定でモジュールは有効になっていません。

そのため、該当するパッケージをspecの`BuildRequires:`に指定していると、パッケージを見つけられず`unresolvable`扱いとなります。

`unresolvable`扱いでは、パッケージのビルドがはじまりません。ウェブサイト側では`nothing provides: xxxx`とそっけないメッセージがでるだけです。

プロジェクトのフラグを設定するには、次のコマンドを実行します。

```console
$ osc meta prjconf home:(ユーザー名)
```

例えば、ruby 3.1を有効にするには次のようにします。[^prjconf]

[^prjconf]: https://openbuildservice.org/help/manuals/obs-user-guide/cha.obs.prjconfig.html

```text
ExpandFlags: module:ruby-3.1
```

#### パッケージのメタ情報を設定する

サンプルとしてhelloパッケージをビルドしてみましょう。
そのためにはパッケージのメタ情報を次のコマンドで設定します。

```console
$ osc meta pkg hello-2.11
```

エディタが起動するので、次のようにパッケージのメタ情報を記述します。

```xml
<package name="hello-2.10" project="home:kenhys">
  <title>Hello 2.10</title>
  <description/>
</package>
```

その後、`osc update`を実行すると対応する`hello-2.10`ディレクトリが作成されます。

#### ソースパッケージをコミットする

パッケージのメタ情報が設定できたら、次はソースパッケージをコミットします。

##### rpmの場合

rpmの場合、specファイルとソースアーカイブを次のようにしてコミットします。

```console
$ cd hello-2.10
$ osc add hello.spec
$ osc add hello-2.10.tar.gz
$ osc commit -m "Add 2.10"
```

##### debの場合

debの場合、dsc等ソースパッケージ一式を次のようにしてコミットします。

```console
$ cd hello-2.10
$ osc add hello_2.10-1.dsc
$ osc add hello_2.10.orig.tar.gz
$ osc add hello_2.10.debian.tar.xz
$ osc commit -m "Add 2.10"
```

なお、`osc-plugin-dput`パッケージをインストールしてあれば、`osc dput hello_2.10-1.dsc`を実行することで上記と同様の結果が得られます。


#### 公開リポジトリからaptやdnfコマンドでパッケージをインストールできるようにする

パッケージのビルドがすべて滞りなく行えたら、しばらくすると公開リポジトリへと反映されます。

##### rpmの場合

プロジェクトでAlmaLinux:8に対応するリポジトリ名`almalinux8`を設定してある場合、既定で.repoファイルが https://download.opensuse.org/repositories/home:/(ユーザー名)/almalinux8/ に公開されるので、`/etc/yum.repo.d`に配置すればよいです。[^mirror-jp]

[^mirror-jp]: ミラーを標準で利用できるようになっており、日本の場合は、mirrorcache-jp.opensuse.orgです。ミラーへの反映は1日程度かかるので、パッケージ開発中に頻繁にコミットしているような場合には、すぐに反映されないことに注意が必要です。

```console
$ cd /etc/yum.repo.d
$ sudo curl -L -O https://mirrorcache-jp.opensuse.org/ \
  repositories/home:/(ユーザー名)/almalinux8/home:(ユーザー名).repo
```

これで、`dnf install`などとして、パッケージを公開リポジトリからインストールできるようになります。

##### debの場合

プロジェクトでDebian:11に対応する対応するリポジトリ名`bullseye`を設定してある場合、既定で`Release.key`が公開されているので、次のようにしてキーリング [^keyring] に変換します。

[^keyring]: パッケージになされた署名の検証に用いるためのものです。ダウンロードされたパッケージが信頼できるものなのかどうかをチェックできます。

```console
$ curl -L -O \
  https://mirrorcache-jp.opensuse.org/repositories/home:/(ユーザー名)/bullseye/Release.key
$ gpg --no-default-keyring --keyring ./archive-hello-keyring.gpg --import Release.key
$ sudo mv archive-hello-keyring.gpg /usr/share/keyrings/
```

そして、変換したキーリングを例えば`/etc/apt/sources.list.d/hello.list`に指定して、公開リポジトリへの参照を設定します。

```text
deb [signed-by=/usr/share/keyrings/archive-hello-keyring.gpg] http://mirrorcache-jp.opensuse.org/repositories/home:(ユーザー名)/(ディストリビューション) /
```

これで、`apt install`などとして、パッケージを公開リポジトリからインストールできるようになります。

## おわりに

今回は、openSUSE Build Serviceを利用してパッケージを公開する方法を紹介しました。
ソースパッケージを準備するところまでできていれば[^source-package]、比較的簡単にリポジトリを公開するところまで実現できることがわかります。

[^source-package]: ソースパッケージを準備するところが大変だという話もありますが、今回の記事からは説明を簡略化するため割愛しました。

個人のプロジェクトで各種ディストリビューション向けのパッケージを提供できるようにするやりかたのみ説明していますが、
個人のプロジェクトとしてではなく、独立したソフトウェア開発ベンダー(ISV)としてプロジェクトを作成してもらうこともできます。[^isv]

[^isv]: isv:配下のネームスペースにプロジェクトを作成できます。ただし通常のワークフローではないため、OBSの管理チームに作業してもらう必要があります。
