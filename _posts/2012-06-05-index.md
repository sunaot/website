---
tags: []
title: ククログの記事のライセンスをCC BY-SA 3.0とGFDLのデュアルライセンスに設定
---
これまで、ククログの記事には特にライセンスを設定していませんでした。そのため、ククログの記事をコピーしたり変更したり再配布したりするためにはクリアコードに許可を求める必要がありました[^0]。
<!--more-->


しかし、それでは記事を再利用したりする敷居が高くなるため、Wikipediaと同じ[CC BY-SA 3.0](http://creativecommons.org/licenses/by-sa/3.0/)[^1]と[GFDL（バージョンなし、変更不可部分なし、表表紙テキストなし、裏表紙テキストなし）](http://www.gnu.org/copyleft/fdl.html)[^2]のデュアルライセンスにし、クリアコードに明示的に許可を求めなくてもライセンスの範囲内で自由に利用できるようにしました。これにより、ククログの記事を以下のように利用できます。

  * 自分のブログにククログの記事を貼り付けることができる。（コピー可）
  * 社内研修の教材に加筆修正したククログの記事を含めることができる。（変更可）
  * ククログの記事を含んだ書籍を販売できる。（商用利用可）
  * ククログの記事を英語に翻訳することができる。（翻訳可）

CC BY-SA 3.0の「原著作者のクレジット」およびGFDLの「著作者」の表記は以下のいずれかを使ってください。

  * 株式会社クリアコード
  * クリアコード
  * ClearCode Inc.

Wikipediaと同様のライセンスとしたので、ククログ固有の不明点はそれほど多くないはずですが、不明点がある場合は[お問い合わせフォーム](/contact/)からご連絡ください。

### まとめ

クリアコードが開発に関わっているソフトウェアと同じように、ククログの記事も自由に利用できるようにしました。この記事と同様の内容は[ライセンスページ](/license/#blog/)にも用意しています。もし、ククログに有用な記事があったら、ぜひ自由に利用してください。

ただし、特定の記事でこのライセンスを適用できないことがある場合は、その記事内[^3]で「この記事のライセンスは○○です。」と明記します。ライセンスに関する記述には注意してください。

[^0]: 求められたことはありません。

[^1]: Creative Commons - Attribution-ShareAlike 3.0 Unportedの略。

[^2]: GNU Free Document Licenseの略。

[^3]: できるだけ記事の先頭。
