---
title: "Fluentd Update - Fluentdとパッケージの最新動向についてOSC 2024 Online/Springで発表しました"
author: kenhys
tags:
- fluentd
- presentation
---

林です。

2024年3月1日・2日に開催された[Open Source Conference 2024 Online/Spring](https://event.ospn.jp/osc2024-online-spring/)において、「Fluentd Update - Fluentdとパッケージの最新動向について」と題した発表を林・福田の両名で行いました。
1日目のC会場にて実施した発表内容を紹介します。

<!--more-->

## 発表資料について

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kenhys/osc2024-online-spring-fluentd/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kenhys/osc2024-online-spring-fluentd/" title="Fluentd Update – Fluentdとパッケージの最新動向について">Fluentd Update – Fluentdとパッケージの最新動向について</a>
  </div>
</div>

* [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/kenhys/osc2024-online-spring-fluentd/)
* [リポジトリー](https://gitlab.com/clear-code/fluentd-support-common/-/tree/main/open-source-conference-2024-online-spring?ref_type=heads)

## 発表内容について

Fluentdプロジェクトのメンバーとして、以前OSC 2022にて次のような発表を行いました。

* [OSSを継続的にメンテナンスしていく仕組みづくり – Fluentdの事例とその最新情報についてOSC 2022 Online Springで発表しました]({% post_url 2022-03-12-osc-2022-spring-fluentd %})

それから2年になるので、Fluentdに関連する最新情報を日本語で提供すべく、登壇しました。
発表においては、次の4つのトピックを話しました。

* Fluentdとは
* OSC2022 Online/Spring以降のできごと
* Fluent Package LTSとは
* Fluentd開発の最新トピック

昨年td-agentの後継となるFluent PackageのLTS版の提供をはじめたのが大きな変化です。
また、直近でリリースする予定のFluent Packageの新バージョンに盛り込む変更点を先行して紹介しました。

当日の発表の内容はYoutubeのOSPN.jpチャンネルのアーカイブで視聴できるようになっています。

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/T0TZ47HLQ1k?si=wqoLWKdJf0B0Gnja" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen>
</iframe>


## さいごに

「Fluentd Update - Fluentdとパッケージの最新動向について」でもお話しましたが、継続的にLTS版をリリースしたり、より活発なコミュニティーサポートにつなげるのが今後の課題としてあります。

OSCでは、あくまでコミュニティーとしての活動内容の話をすることが目的なので言及していませんでしたが、クリアコードでは、[Fluentdのサポートサービス]({% link services/fluentd.md %})を提供しています。サポートサービスでは障害発生時の調査や回避策の提案、パッチの提供などを行います。
また、既存のtd-agent v4からFluent Packageへのアップグレードの支援もサポートサービスの一環として行っています。
Fluentdに関するトラブルを抱えて困っている方は、ぜひこちらの[お問い合わせフォーム]({% link contact/index.md %})からご連絡ください。
