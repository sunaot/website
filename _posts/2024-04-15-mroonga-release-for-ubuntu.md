---
title: 'Mroonga: Ubuntuのパッケージをリビルドして公開する方法'
author: abetomo
tags:
- groonga
---

阿部です。

Mroongaのパッケージ提供やLaunchpadの利用を初体験したので、
その経験のまとめ記事です。

具体的にはMroongaをUbuntu向けにリビルドしてパッケージ提供する手順の記事です。
今回はMySQL 8.0向けのパッケージの例です。

バッチが整備されているので実行の手順と、バッチの中身の簡単な解説記事です。

（Launchpadでパッケージ提供する方法を1から解説記事ではありません。
その解説記事は[こちら]({% post_url 2014-06-10-index %})）

<!--more-->

## 経緯と対応

以下のエラーでMroongaがインストールできないと[報告がありました](https://matrix.to/#/!rZemGiqmyMCWkAxURV:gitter.im/$D78m-JIdEqCIiG3DHOr_pAdwtKO9eLElLNJTIL_iu9g?via=gitter.im&via=matrix.org&via=matrix.fedibird.com)。

```console
$ sudo apt-get install -y -V mysql-server-mroonga
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
Some packages could not be installed. This may mean that you have
requested an impossible situation or if you are using the unstable
distribution that some required packages have not yet been created
or been moved out of Incoming.
The following information may help to resolve the situation:

The following packages have unmet dependencies:
 mysql-mroonga : Depends: mysql-server-8.0-mroonga (= 13.05-2.ubuntu22.04.3) but it is not going to be installed
                 Depends: mysql-server (= 8.0.35-0ubuntu0.22.04.1) but it is not going to be installed
```

エラーの原因はUbuntuの最新のmysql-serverパッケージ向けにリビルドした
Mroonga（`mysql-server-8.0-mroonga` パッケージ）を提供していないことです。

そういうわけでUbuntuの最新のmysql-serverパッケージ向けにリビルドして提供します。

リビルドはLaunchpadが実施してくれるので、実際にやることはLaunchpadにソースコードを
アップロードすることです。

以降でその手順を説明します。

## 事前準備

これらの準備は終わっているものとして具体的な手順などは割愛します。

### Launchpad

Launchpadは利用できる状態。

[参考: Launchpadを利用してパッケージを公開するには]({% post_url 2014-06-10-index %})

* Launchpadへのユーザー登録
* Launchpadへの公開鍵の登録
* CoCへの署名
* チームへのユーザー登録

### 必須コマンド

必要なコマンドはインストールしておきます。

`dch`、`dh` コマンドが必要です。

Ubuntuでインストールするコマンド例:

```bash
sudo apt install -y -V devscripts debhelper
```

### Groongaリポジトリ

Groongaに定義されている `rake` タスクを利用するので、Groongaリポジトリをcloneしておきます。

コマンド例:

```bash
git clone https://github.com/groonga/groonga.git
```

### Mroongaリポジトリ

本題のリポジトリです。当然必要なのでcloneします。

コマンド例:

```bash
git clone https://github.com/mroonga/mroonga.git
```

## 手順と各タスクの説明

### 手順概要

1. ソースコードのダウンロード
   * A. メジャー・マイナーリリースの場合
   * B. パッチリリースの場合
2. `debian/changelog` の更新
3. Launchpadへソースコードをアップロード
4. Launchpadでビルドから公開まで待つ
5. `debian/changelog` のコミット

### 1-A. ソースコードのダウンロード （メジャー・マイナーリリースの場合）

`source:download` タスクを実行します。
このタスクは
`https://github.com/mroonga/mroonga/releases/download/v{VERSION}/mroonga-{VERSION}.tar.gz`
からファイルをダウンロードして、ダウンロードしたファイルに対して
`gpg --local-user xxx --armor --detach-sign mroonga-{VERSION}.tar.gz`
を実行します。

コマンド例:

```bash
cd <Mroongaリポジトリのルートディレクトリ>/packages/source/
GROONGA_REPOSITORY=${GROONGAのソースのパス} \
  rake source:download \
  VERSION=13.05
```

Groongaに定義されている `rake` タスクを利用するので、Groongaリポジトリを
`GROONGA_REPOSITORY` で指定します。
また、リビルドに使うバージョンを `VERSION` で指定します。

`<Mroongaリポジトリのルートディレクトリ>/source/repositories/source/mroonga/` にダウンロードされるので、
`mroonga-X.Y.tar.gz` をMroongaリポジトリのルートディレクトリに
配置します。

```bash
mv source/repositories/source/mroonga/mroonga-13.05.tar.gz ../../
```

このあとのタスクでMroongaリポジトリのルートディレクトリに配置した `tar.gz` から
`<Mroongaリポジトリのルートディレクトリ>/packages/mysql-8.0-mroonga/mysql-8.0-mroonga-X.Y.tar.gz`
を生成します。

### 1-B. ソースコードのダウンロード（パッチリリースの場合）

バージョンが同じ（`VERSION=X.Y` が同じ） ソースコードがすでにアップロード済の場合は、
前にビルドしたときのソースアーカイブをダウンロードして配置します。

例: https://launchpad.net/~groonga/+archive/ubuntu/ppa/+sourcefiles/mysql-8.0-mroonga/13.05-2.ubuntu22.04.3/mysql-8.0-mroonga_13.05.orig.tar.gz

ダウンロードしたファイルを
`<Mroongaリポジトリのルートディレクトリ>/packages/mysql-8.0-mroonga/mysql-8.0-mroonga-X.Y.tar.gz`
に直接配置します。

### 2. `debian/changelog` の更新

`<Mroongaリポジトリのルートディレクトリ>/packages/mysql-8.0-mroonga/` へ移動し、
`version:update` タスクを実行します。

コマンド例:

```bash
cd packages/mysql-8.0-mroonga/
GROONGA_REPOSITORY=${GROONGAのソースのパス} \
  rake version:update \
  VERSION=13.05 \
  DEB_RELEASE=3
```

このタスクは下記の `changelog` にリリース情報を追記します。

```
<Mroongaリポジトリのルートディレクトリ>/packages/mysql-8.0-mroonga/debian/changelog
```

変更の例:

```diff
diff --git a/packages/mysql-8.0-mroonga/debian/changelog b/packages/mysql-8.0-mroonga/debian/changelog
index 6a31d8fa..85f93725 100644
--- a/packages/mysql-8.0-mroonga/debian/changelog
+++ b/packages/mysql-8.0-mroonga/debian/changelog
@@ -1,3 +1,9 @@
+mysql-8.0-mroonga (13.05-3) unstable; urgency=low
+
+  * Rebuilt against MySQL 8.0.35.
+
+ -- Abe Tomoaki <abe@clear-code.com>  Fri, 16 Feb 2024 05:45:42 -0000
+
 mysql-8.0-mroonga (13.05-2) unstable; urgency=low

   * Rebuilt against MySQL 8.0.34.
```

コマンドで指定した `DEB_RELEASE` は `13.05-3` の最後の「3」の指定で利用されます。

この `changelog` は必要に応じて修正します。

### 3. Launchpadへソースコードをアップロード

ここまで準備できたらLaunchpadへソースコードをアップロードします。

`ubuntu:upload` タスクでアップロードします。

コマンド例:

```bash
GROONGA_REPOSITORY=${GROONGAのソースのパス} \
  LAUNCHPAD_UPLOADER_PGP_KEY=<公開鍵のハッシュ値> \
  rake ubuntu:upload \
  VERSION=13.05
```

（このタスクで `dch`、`dh` コマンドを使います。）

具体的にどのような処理をしているかは
[Launchpadを利用してパッケージを公開するには > パッケージの作成とアップロード]({% post_url 2014-06-10-index %}#%E3%83%91%E3%83%83%E3%82%B1%E3%83%BC%E3%82%B8%E3%81%AE%E4%BD%9C%E6%88%90%E3%81%A8%E3%82%A2%E3%83%83%E3%83%97%E3%83%AD%E3%83%BC%E3%83%89)
に解説がありますので、ご参照ください。

（参照している記事に登場するコードの最新は[こちら](https://github.com/groonga/groonga/blob/v14.0.2/packages/launchpad-helper.rb#L87-L129)です。）

アップロードが終わると成否がメールで届きます。

* 成功メールのタイトル例: `[~groonga/ubuntu/ppa/jammy] mysql-8.0-mroonga 13.05-3.ubuntu22.04.4 (Accepted)`
* 失敗メールのタイトル例: `[~groonga/ubuntu/ppa] mysql-8.0-mroonga_13.05-3.ubuntu22.04.1_source.changes (Rejected)`

### 4. Launchpadでビルドから公開まで待つ

Launchpadへのソースコードをアップロードに成功すると、Launchpadがビルドして公開までしてくれます。
Ubuntuのバージョンごとにビルドして公開してくれます。

Launchpadでビルドが問題なく完了すれば `status` が `Published` になります。

以上でパッケージは公開されました。

### 5. `debian/changelog` のコミット

最後に変更した
`<Mroongaリポジトリのルートディレクトリ>/packages/mysql-8.0-mroonga/debian/changelog`
をコミットして完了です。

## まとめ

MySQL 8.0向けのMroongaをUbuntu向けにリビルドしてパッケージ提供する手順を書きました。

## 参考: タスク定義

* [Groongaのタスク定義](https://github.com/groonga/groonga/blob/v14.0.2/packages/packages-groonga-org-package-task.rb)
  * [Groongaのタスクで利用しているLaunchpadへのアップロード関連タスク](https://github.com/groonga/groonga/blob/v14.0.2/packages/launchpad-helper.rb)
  * [Groongaのタスク定義で読み込んでいるApache Arrowのタスク定義](https://github.com/apache/arrow/blob/apache-arrow-15.0.2/dev/tasks/linux-packages/package-task.rb)
* [Mroongaのタスク定義](https://github.com/mroonga/mroonga/blob/v13.05/packages/mroonga-package-task.rb)
  * 主にMroonga専用の設定がしてある
