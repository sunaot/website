---
tags: []
title: Debianでパッケージをリリースできるようにしたい - mentors.debian.netの使いかた
---
### はじめに

以前、オープンソースのカラムストア機能付き全文検索エンジンである[Groonga](http://groonga.org/ja)をDebianに入れるために必要な作業について、最初のとっかかりであるWNPPへのバグ登録やDebianらしいパッケージかどうかチェックするためのLintianについて紹介記事を書きました。
<!--more-->


  * [Debianでパッケージをリリースできるようにしたい - WNPPへのバグ登録]({% post_url 2014-03-07-index %})
  * [Debianでパッケージをリリースできるようにしたい - よりDebianらしく]({% post_url 2014-04-03-index %})

今回は、現在進行中であるGroongaのDebianリポジトリ入りを目指す作業の中から、Debianパッケージのアップロード先となる[mentors.debian.net](http://mentors.debian.net)の使いかたについて紹介します。

### mentors.debian.netとは

WNPPにバグ登録をしたのち、debパッケージを用意するところまでできたら、次はパッケージを公開します。これはスポンサーに作ったパッケージをレビューしてもらう必要があるためです。

Groongaの場合には、Debian開発者である[やまね](https://wiki.debian.org/HidekiYamane)さんにスポンサーしてもらえることになっていたので、スポンサー探しに苦労することはありませんでした。

パッケージのレビューをしてもらうときに、パッケージのアップロード先として利用するのがmentors.debian.netです。パッケージをアップロードするには次のような手順を踏みます。

  * mentors.debian.netにアカウントを作成する（初回のみ）
  * アップロードに必要な設定をする（初回のみ）
  * パッケージをmentors.debian.netにアップロードする

### mentors.debian.netにアカウントを作成する（初回のみ）

まずはmentors.debian.netにアカウントを作成します。そして、パッケージの署名に使うGPGの鍵を登録します。登録する際は次のコマンドを実行した結果を[アカウント詳細](https://mentors.debian.net/my)ページにてアップロードします。

{% raw %}
```
$ gpg --export --export-options export-minimal --armor （鍵のID）
```
{% endraw %}

アカウントの登録が済んだら、次はアップロードに必要な初期設定をします。

### アップロードに必要な設定をする（初回のみ）

パッケージをmentors.debian.netへとアップロードするにはdputを使います。アップロード先の設定は、~/.dput.cfに行います。

{% raw %}
```
[mentors]
fqdn = mentors.debian.net
incoming = /upload
method = http
allow_unsigned_uploads = 0
progress_indicator = 2
# Allow uploads for UNRELEASED packages
allowed_distributions = .*
```
{% endraw %}

これで、パッケージをアップロードするための準備が整いました。次はパッケージをアップロードします。

### パッケージをmentors.debian.netにアップロードする

.dput.cfの設定ができたらパッケージをアップロードします。その際には、引数としてアップロード先であるmentorsと.changesファイルを指定します。

{% raw %}
```
$ dput mentors ../groonga_4.0.0-1_amd64.changes
Checking signature on .changes
gpg: Signature made Mon 10 Feb 2014 08:59:20 AM UTC using RSA key ID 3455D448
gpg: Good signature from "HAYASHI Kentaro <hayashi@clear-code.com>"
Good signature on ../groonga_4.0.0-1_amd64.changes.
Checking signature on .dsc
gpg: Signature made Mon 10 Feb 2014 08:59:13 AM UTC using RSA key ID 3455D448
gpg: Good signature from "HAYASHI Kentaro <hayashi@clear-code.com>"
Good signature on ../groonga_4.0.0-1.dsc.
Uploading to mentors (via ftp to mentors.debian.net):
  Uploading groonga_4.0.0-1.dsc: done.
  Uploading groonga_4.0.0.orig.tar.gz: done.
  Uploading groonga_4.0.0-1.debian.tar.xz: done.
  Uploading groonga_4.0.0-1_amd64.deb: done.
  Uploading groonga-server-common_4.0.0-1_amd64.deb: done.
  Uploading groonga-server-http_4.0.0-1_amd64.deb: done.
  Uploading groonga-server-gqtp_4.0.0-1_amd64.deb: done.
  Uploading libgroonga-dev_4.0.0-1_amd64.deb: done.
  Uploading libgroonga0_4.0.0-1_amd64.deb: done.
  Uploading groonga-tokenizer-mecab_4.0.0-1_amd64.deb: done.
  Uploading groonga-plugin-suggest_4.0.0-1_amd64.deb: done.
  Uploading groonga-bin_4.0.0-1_amd64.deb: done.
  Uploading groonga-httpd_4.0.0-1_amd64.deb: done.
  Uploading groonga-doc_4.0.0-1_all.deb: done.
  Uploading groonga-examples_4.0.0-1_all.deb: done.
  Uploading groonga-munin-plugins_4.0.0-1_amd64.deb: done.
  Uploading groonga_4.0.0-1_amd64.changes: done.
Successfully uploaded packages.
```
{% endraw %}

<div class="callout alert">
2023/08追記: 現在では、mentors.debian.netへのバイナリパッケージのアップロードは推奨されていない( <a href="https://mentors.debian.net/qa/">My binary packages do not show up</a> に言及あり) ため、ソースパッケージのアップロードをする必要があります。
つまり、dput mentors ../groonga_4.0.0-1_source.changesなどとしてアップロードします。
</div>

アップロードに成功してしばらくすると、パッケージごとの個別ページが参照できます。パッケージのページではLintianによるチェック結果を参照できます。 具体的な結果については [Groongaの例](http://mentors.debian.net/package/groonga) が参考になるでしょう。

#### 間違ったときの対処について

もしdputしたときに、アップロード先としてmentorsの指定を忘れるとどうなるでしょうか。そのときはdputのデフォルトのアップロード先にアップロードされてしまいます。/etc/dput.cfの[DEFAULT]欄が何も指定していなかったときのアップロード先です。

その場合には、dcutコマンドを使って取り消します。dcutコマンドを使うと削除に必要なコマンドファイルをアップロードします。アップロード先では、定期的にコマンドファイルを処理しているので、しばらくするとコマンドファイルに従ってファイルを削除してくれます。

例えば、間違ってデフォルトの設定でアップロードしてしまった場合、次のようにしてdcutで取り消します[^0]。

{% raw %}
```
$ dcut -m hayashi@clear-code.com -i groonga_4.0.0-1_amd64.changes
 signfile /tmp/dcut.jNuDdW/dcut.hayashi_clear_code_com.1392865192.3377.commands hayashi@clear-code.com

You need a passphrase to unlock the secret key for
user: "HAYASHI Kentaro <hayashi@clear-code.com>"
4096-bit RSA key, ID 3455D448, created 2012-04-24

gpg: gpg-agent is not available in this session

Successfully signed commands file
  Uploading dcut.hayashi_clear_code_com.1392865192.3377.commands: done.
```
{% endraw %}

ただし、上記ができるのは、間違ってアップロードしてしまったときの.changesが残っている場合です。うっかり.changesを削除してしまった場合には使えません。デフォルトのアップロード先がDebian公式のUploadQueueだったため、このミスをしたときは[ftp-master](https://wiki.debian.org/Teams/FTPMaster)に削除を依頼する必要がありました。

### まとめ

今回はdebパッケージをスポンサーにレビューしてもらうときに利用するmentors.debian.netの使いかたを紹介しました。パッケージをmentors.debian.netにアップロードしたら、スポンサーにレビューしてもらう必要があります。スポンサーによる指摘事項があれば都度修正して、きちんとしたパッケージに仕上げていく作業が必要です。それらについては、またの機会に記事にします。

[^0]: -mオプションにはGPGの鍵にひもづいたメールアドレスを指定します。
