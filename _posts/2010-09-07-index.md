---
tags:
- milter-manager
title: 'お知らせ: hbstudy#15: milter managerで簡単迷惑メール対策'
---
お知らせです。
<!--more-->


来週の週末9/18（土）の19:00-開催される[hbstudy#15](http://heartbeats.jp/hbstudy/2010/08/hbstudy15.html)で[milter manager](/software/milter-manager.html)を紹介します。

hbstudyはインフラエンジニアのための勉強会で、#15では迷惑メール対策ソフトウェアmilter managerと監視ソフトウェア[Zabbix](http://www.zabbix.com/jp/)がテーマです。同じ回で違う分野を扱うので、知識の幅が広がりやすいのがよいですね。

「複数のmilterを同時に使ったときの挙動を理解できること」を目標に話す予定です。まだ少し空いているようなので興味のある方は参加してみてください。

  * [hbstudy#15 : ATND](http://atnd.org/events/7578)
  * [hbstudy#15懇親会 : ATND](http://atnd.org/events/7579)
