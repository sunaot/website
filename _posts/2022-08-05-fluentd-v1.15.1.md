---
title: Fluentd v1.15.1リリース -- プラグイン共通の排他機能の追加
author: daipom
tags:
  - fluentd
---

2022年7月27日にFluentdの最新版となるv1.15.1をリリースしました。

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加し、リリースを含めたメンテナンス作業を行っています。

v1.15.1では、`out_file`に関する修正を行いましたが、それに伴い共通機能として排他機能を追加しました。
この新機能を利用頂くことで、Outputプラグインをマルチワーカーに簡単に対応させられるようになりました。

今回は、この修正について詳しくご紹介します。

* [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#release-v1151---20220727)
* [リリースアナウンス](https://www.fluentd.org/blog/fluentd-v1.15.1-has-been-released)

<!--more-->

## `out_file`: マルチワーカーで同じファイルへ追記すると、稀にレコードが破損する問題を修正

[マルチワーカーの機能](https://docs.fluentd.org/deployment/multi-process-workers)は複数のワーカープロセスを動作させることで、
マシンのスペックを活用した高トラフィックな処理を可能にする強力な機能です。
しかし一方で複数のプロセスが同時に動作するため、プラグインをこの機能に対応させるには、プロセス間で処理が競合しないように注意して設計する必要があります。

今回、マルチワーカーで動作する`out_file`が同じタイミングで同じファイルへ追記する場合に、
稀に書込処理が競合してレコードが破損する現象([Issue#3805](https://github.com/fluent/fluentd/issues/3805))を発見し、修正しました。

### 発見の経緯

[out_fileのappendオプション](https://docs.fluentd.org/output/file#append)を`true`に設定すると、既に出力先にファイルが存在する場合に追記を行います[^append]。
この動作をマルチワーカーで行わせると、書込処理が競合してレコードが破損する可能性がありました。

実際にこの問題が発生した訳ではなく、偶然`out_file`の実装を眺めていて疑問に思い、検証を行ったのが発端でした。
大量に書込を行わせても一向に再現しなかったのですが、[Issue#3805](https://github.com/fluent/fluentd/issues/3805)の手順でFluentdの停止時に溜まったbufferを一斉にflushさせたところ、
再現させることができました。
この手順では、18240行のうち26行の破損が確認できました。

以上の検証から、この現象は非常にレアなケースだと考えていますが、これを機会に抜本的な修正を行いました。

[^append]: デフォルトは`false`で、既に同名のファイルが存在する場合に連番のsuffixを付与して新しいファイルとして出力する動作になります。

### プラグイン共通の排他機能の追加

修正は[PR#3808](https://github.com/fluent/fluentd/pull/3808)で行いました。

Fluentdの各プラグインの基底クラスに排他機能を追加したので、`out_file`以外のプラグインでも共通して利用できる機能になっています。

具体的には、次のように`acquire_worker_lock`メソッドを追加しました。

* https://github.com/fluent/fluentd/blob/v1.15.1/lib/fluent/plugin/base.rb#L79-L90

一時ディレクトリ上で指定された名前のファイルをロックファイルとして利用することで排他を行います。
`out_file`プラグインでは、このメソッドを次のように使用しています。

* https://github.com/fluent/fluentd/blob/v1.15.1/lib/fluent/plugin/out_file.rb#L225-L227

このようにすることで、各プラグインで簡単に安全なファイル書込を行うことができます。

自前ロジックでマルチワーカーに対応しているOutputプラグインや、まだ対応できていないOutputプラグインを使用されている方は、
ぜひこの新機能を使ってみて下さい！

## まとめ

今回の記事では、Fluentd v1.15.1について最新情報をお届けしました。

最新版を使ってみて、何か気になる点があればぜひ[GitHub](https://github.com/fluent/fluentd/issues)で開発チームまでフィードバックをお寄せください！

また、クリアコードはFluentdなどの自由なソフトウェアの開発・サポートを行っております。自分も仕事でFluentdの開発をしたい！という方は、[クリアコードの採用情報]({% link recruitment/index.md %})をぜひご覧ください。
