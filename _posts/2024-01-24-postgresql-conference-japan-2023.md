---
tags:
- apache-arrow
- presentation
title: "PostgreSQL Conference Japan 2023 - Apache Arrow Flight SQLでPostgreSQLをもっと速く！ #pgcon23j"
author: kou
---

なんともう2ヶ月前になってしまったのですが、[PostgreSQL Conference Japan 2023](https://www.postgresql.jp/jpug-pgcon2023)で[Apache Arrow Flight SQLでPostgreSQLをもっと速く！](https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-japan-2023/)という話をしてきた須藤です。

<!--more-->

<div class="rabbit-slide rabbit-slide-wide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-japan-2023/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; box-sizing: content-box; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-japan-2023/" title="Apache Arrow Flight SQLでPostgreSQLをもっと速く！">Apache Arrow Flight SQLでPostgreSQLをもっと速く！</a>
  </div>
</div>

関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/postgresql-conference-japan-2023/)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-postgresql-conference-japan-2023)

### 内容

2023年はPostgreSQLを[Apache Arrow Flight SQL](https://arrow.apache.org/blog/2022/02/16/introducing-arrow-flight-sql/)に対応させる[Apache Arrow Flight SQL adapter for PostgreSQL](https://arrow.apache.org/flight-sql-postgresql/current/)というプロダクトを開発していました。ということで、このプロダクトがどんな問題をどのように解決するプロダクトなのかというユーザー目線での説明と、このプロダクトの実装はどうなっているのかという開発者目線での説明をしました。

このプロダクトがどんな問題を解決したいかというと次の2つです。

* PostgreSQL*から*の大量データの*読み*込みが遅い
* PostgreSQL*へ*の大量データの*書き*込みが遅い

これらの問題をApache ArrowベースのプロトコルであるApache Arrow Flight SQLで解決するのがこのプロダクトです。

Apache Arrowはデータ交換コストをほぼ0にするデータフォーマットです。PostgreSQLのプロトコルが使っているデータフォーマットはデータ交換コストがほぼ0ではないのでApache Arrowフォーマットを使うことで高速化できないかという狙いです。

現在のPostgreSQLではPostgreSQLプロトコルでApache Arrowフォーマットを使えないので、プロトコルごと変えてApache Arrowフォーマットを使おうというのがこのプロダクトです。プロトコルにはApache ArrowとSQLを使った高速なプロトコルApache Arrow Flight SQLを使います。

この問題を解決できたかどうかは高速になったかどうかがポイントなのでいくつかのパターンで検証しました。しかし、残念ながら思ったほど速くなっていませんでした。。。いくつかのパターンでは速かったのですがそれ以外のパターンはむしろ遅くなっていました。高速化の案はあるので引き続き高速化に取り組んでいきます。

開発者目線では次のことを説明しました。

* 認証まわりの実装方法
* PostgreSQLに新しくプロトコルを追加する方法

このプロダクトの開発に興味がある人は https://github.com/apache/arrow-flight-sql-postgresql に来てください。一緒に開発しましょう！

### オフラインカンファレンスのメリット

PostgreSQL Conference Japanは例年オフラインで開催されています。私は数年ぶりに参加しましたが、やはりオフラインカンファレンスはいいですね！

私は聞いてくれている人たちの様子を見ながら話す方が楽しいのでオフラインの方が楽しく話せます。今回は特に楽しく話せました。今回はスタッフの方から「コミュニティーっぽい話し方でいいですね」というようなコメントをもらったのですが、そういう感じ（どういう感じ？）の話し方をしているんでしょう。ただ、私の話し方はちょっとクセがあるので苦手な人もいるとは思います。実際、アンケートで「良くなかった」（5段階評価で最低評価）がついていた数少ない講演でした。でも、私はこの話し方以外はできないんですよねぇ。違う話し方だと私が楽しくなくなってしまいそうです。すみませんねぇ。

話しているときだけではなく、話していないときもオフラインでのいいことがあります。RubyKaigiもそうなのですが、開発者がそこらへんを歩いているのがいいです。今回の話に関連するところでは、PostgreSQLの`COPY`を拡張できないかなぁというアイディアがありました。`COPY`でApache Arrowフォーマットを扱うことができればPostgreSQLプロトコルでも高速にできそうだからです。ということで、そこらへんにいた[@masahiko_sawada](https://twitter.com/masahiko_sawada)や[@fujii_masao](https://twitter.com/fujii_masao)や[@michaelpq](https://twitter.com/michaelpq)が相談にのってくれました。とても助かりました。ありがとうございました！

その結果、すでにそういう議論がされていて`COPY`を拡張できるようにしようよ、というところまで話が進んでいることがわかりました。ただ、誰も実装していなくて話がそこで止まっていました。後は仕様を詰めて実装するだけだったので、PostgreSQL Conference Japan 2023が終わってからはpgsql-hackersでそこらへんのことをやっていました。[Make COPY format extendable: Extract COPY TO format implementations](https://www.postgresql.org/message-id/flat/20231204.153548.2126325458835528809.kou%40clear-code.com)のスレッドでやりとりしたりパッチを投げたりしているので、興味がある人はレビューしたり動作確認したりしてね。

### まとめ

なんともう2ヶ月前になってしまったPostgreSQL Conference Japan 2023で話したことについて書きました。

久しぶりに参加したのですがいいカンファレンスでした！今年は「`COPY`を拡張可能にしたぞ！」という話をしに行けるといいな。
