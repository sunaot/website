---
title: 障害復旧機能を提供するcollectdプラグインの紹介(ローカル監視機能編)
author: kenhys
tags:
  - use-case
---

システムやアプリケーションのメトリクス情報をあつめるソフトウェアの1つに[collectd](https://collectd.org/)があります。

今回は、[株式会社セナネットワークス様](https://www.sena-networks.co.jp/)からの発注を受けて開発した障害復旧機能を提供するcollectdプラグイン(lua-collectd-monitor)について紹介します。

<!--more-->

[lua-collectd-monitor](https://github.com/clear-code/lua-collectd-monitor)は障害復旧機能を提供するcollectdプラグインです。
リモートからリカバリコマンドを受け取って実行したり、ローカルのメトリクスデータをもとにリカバリコマンドを実行できます。

まずはローカル監視機能の導入手順と使い方を説明します。

### 動作環境について

lua-collectd-monitorはcollectdのLuaプラグインに機能追加してあることを前提としています。
upstreamにはまだフィードバックした内容が取り込まれていないため、パッチをあてたcollectdが必要です。
CentOS 8であれば、パッチを適用済みのパッケージがあるのでそちらを使うのがおすすめです。

* [CentOS 8向けパッケージ](https://github.com/clear-code/collectd/releases/tag/5.12.0.16.g6e9604f)

Ubuntuを使っている人のためにUbuntu 20.04の導入例を説明します。

* 必要なパッケージをインストールする
* lua-collectd-monitorをインストールする
* 必要に応じてカスタマイズする

### 必要なパッケージをインストールする

```sh
$ sudo add-apt-repository ppa:kenhys/nightly
$ sudo apt-get update
$ sudo apt install lua5.3 liblua5.3-dev luarocks collectd-core libssl-dev make
```

これで collectd-core 5.9.2.g-1ubuntu5.1がインストールされます。

```sh
$ dpkg -l | grep collectd
ii  collectd-core                        5.9.2.g-1ubuntu5.1                amd64        statistics collection and monitoring daemon (core system)
```

### lua-collectd-monitorをインストールする


```sh
$ git clone https://github.com/clear-code/lua-collectd-monitor
$ cd lua-collectd-monitor
$ sudo luarocks make
```

インストールできたら サンプルの`collectd.conf`をコピーします。

```sh
$ sudo cp /usr/share/doc/collectd-core/examples/collectd.conf /etc/collectd/
```

次に、`/etc/collectd/collectd.conf.d/lua-monitor-local.conf` を以下の内容で作成します。

```aconf
LoadPlugin syslog
LoadPlugin memory

<LoadPlugin lua>
    Globals true
</LoadPlugin>

<Plugin syslog>
    LogLevel info
    NotifyLevel OKAY
</Plugin>

<Plugin lua>
    BasePath "/usr/local/share/lua/5.3"
    Script "collectd/monitor/local.lua"
    <Module "collectd/monitor/local.lua">
        MonitorConfigPath "/etc/collectd/monitor/config.json"

        # This option can be placed in above config.json too.
        # If there is same option in both files, config.json's one is applied.
        LocalMonitorConfigDir "/etc/collectd/monitor/local/"
    </Module>
</Plugin>
```

最後にローカル監視機能をカスタマイズするための設定とリカバリコマンドの雛形をコピーします。

```sh
$ sudo cp -r conf/collectd/monitor/ /etc/collectd/
```

あとはcollectdを再起動すればインストール完了です。

### 必要に応じてカスタマイズする

lua-collectd-monitorのカスタマイズは次の2つを修正することで行います。

* 障害復旧対象のサービスの定義
* メトリクスから障害を検知するためのしきい値を定義

前者は `/etc/collectd/monitor/config.json` で後者が `/etc/collectd/monitor/local/example.lua` です。

`/etc/collectd/monitor/local`以下のスクリプト名は拡張子が`.lua`であることを除いて任意です。

config.jsonは次のような内容です。

```json
{
  "Host": "localhost:1883",
  "User": "test-user",
  "Password": "test-user",
  "Secure": false,
  "CleanSession": false,
  "ReconnectInterval": 5,
  "QoS": 2,
  "CommandTopic": "test-topic",
  "CommandResultTopic": "test-result-topic",
  "Services": {
    "nginx": {
      "commands" : {
        "restart": "/bin/systemctl restart nginx 2>&1"
      }
    },
    "hello": {
      "commands": {
        "exec": "/bin/echo \"Hello World!\""
      }
    }
  }
}
```

復旧対象のサービスとして`nginx`と`hello`が登録されています。
また、`nginx`には`restart`コマンドが、`hello`には`exec`コマンドが定義されていることがわかります。
`Services` 以外にもいろいろ項目がありますが、それらはリモート監視のための設定項目なので今回は関係ありません。

一方の`example.lua`にはプラグインが受け取ったメトリクスを使って、どのサービスを実行するかという定義をしています。

例えば、メモリの空き容量が50MBを下回ったときにnginxを再起動するには次のような定義をします。

```lua
local metric_handlers = {
   memory_free_is_under_50MB = function(metric)
      if metric.plugin == "memory" and metric.type_instance == "free" then
         if metric.values[1] <= 50 * 1000 * 1000 then
            return { service = "nginx", command = "restart" }
         end
      end
      return nil
   end
}
local notification_handlers = {
  ...
}

return metric_handlers, notification_handlers
```

しきい値である50MBを下回った場合に実行するサービスとそのコマンドをペアで返すようになっています。
そうすることで、この場合は`nginx`というサービスの`restart`コマンドが実行される仕組みになっています。

### まとめ

今回は障害復旧機能を提供するcollectdプラグインにて、ローカル監視機能を使う方法について説明しました。
機会があれば、もうひとつのリモートコマンドを実行する機能についても解説したいと思います。
