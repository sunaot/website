---
tags:
- mozilla
title: CPUの使用率とメモリの使用量を表示するFirefoxアドオン「システムモニター」を更新しました
---
システムの情報をFirefoxのツールバー上に表示するアドオン「システムモニター」のバージョン0.5をリリースしました。以下のリンク先からダウンロードできます。
<!--more-->


  * [packages.clear-code.comからダウンロード](http://packages.clear-code.com/xul/extensions/system-monitor/system-monitor-0.5.xpi)
  * [addons.mozilla.orgからダウンロード](https://addons.mozilla.org/firefox/addon/15093)
  * [Gitリポジトリ](http://git.clear-code.com/xul/extensions/system-monitor/)

前のバージョンからの主な変更点は以下の通りです。

  * マルチCPUの環境で、個々のCPUの使用率の表示に対応しました。
  * グラフの表示形式として、折れ線グラフを選択できるようにしました。

### 個々のCPUの使用率のグラフ

マルチCPUに対応するにあたって、グラフの表示形式を追加しました。これらの表示様式は設定ダイアログで切り替える事ができます。

![CPUごとの使用率の棒グラフ（重ね合わせ表示）]({{ "/images/blog/20101019_2.png" | relative_url }} "CPUごとの使用率の棒グラフ（重ね合わせ表示）")
重ね合わせ表示（既定の表示スタイル）は、それぞれのCPUについて下端が0%・上端が100%となるグラフを表示し、それらを全て重ね合わせるモードです。

![CPUごとの使用率の棒グラフ（積み上げ表示）]({{ "/images/blog/20101019_1.png" | relative_url }} "CPUごとの使用率の棒グラフ（積み上げ表示）")
積み上げ表示は、グラフの高さをCPUの個数分で割って、それぞれを個々のCPUの使用率の0〜100%に割り当てるモードです。

![全CPUの使用率の合計の棒グラフ]({{ "/images/blog/20101019_0.png" | relative_url }} "全CPUの使用率の合計の棒グラフ")
全CPUの使用率の合計を表示するよう指定した場合、旧バージョンと同じ表示になります。

また、棒グラフの代わりに折れ線グラフでも表示できるようになりました。

![CPUごとの使用率の折れ線グラフ（重ね合わせ表示）]({{ "/images/blog/20101019_5.png" | relative_url }} "CPUごとの使用率の折れ線グラフ（重ね合わせ表示）")
![CPUごとの使用率の折れ線グラフ（積み上げ表示）]({{ "/images/blog/20101019_4.png" | relative_url }} "CPUごとの使用率の折れ線グラフ（積み上げ表示）")
![全CPUの使用率の合計の折れ線グラフ]({{ "/images/blog/20101019_3.png" | relative_url }} "全CPUの使用率の合計の折れ線グラフ")

ただ、ツールバーに表示するUIでは表示領域が限られますので、CPUの数が増えてくると見にくくなるかもしれません。次のバージョンではこのあたりの問題についてうまい解決策を考えたい所です。

![クアッドコアの環境での動作]({{ "/images/blog/20101019_6.png" | relative_url }} "クアッドコアの環境での動作")

### Web APIで個々のCPUの使用率を取得する方法

Web APIの利用形式は[前のバージョン]({% post_url 2010-10-05-index %})と同様ですが、このバージョンでは新たに、system.addMonitorの第1引数として「cpu-usages」と「cpu-times」を受け付けるようになりました。[「cpu-usage」および「cpu-time」を使用した場合]({% post_url 2009-10-20-index %})は今まで通り全CPUの使用率の合計値がリスナに渡されますが、「cpu-usages」および「cpu-times」を使用した場合は、個々のCPUごとの値が格納された配列が渡されます。

以下は、CPUごとの使用率を監視する例です。マルチCPUの環境では、グラフが重ねて描画される事をご確認いただけると思います。

{% raw %}
```html
<div id="system-monitor-multi-cpu-demo"></div>
<script type="text/javascript"><!--
var container = document.getElementById("system-monitor-multi-cpu-demo");
if (!window.system || !window.system.addMonitor) {
  container.innerHTML = "システムモニターがインストールされていません";
  container.style.color = "red";
} else {
  container.innerHTML = "<div><canvas id='system-monitor-multi-cpu' width='300' height='60'></canvas>"+
                        "<br /><span id='system-monitor-multi-cpu-console'></span></div>";

  var width = 300;
  var interval = 1000;

  var CPUArray = [];
  var arrayLength = width / 2;
  while (CPUArray.length < arrayLength) {
    CPUArray.push(undefined);
  }

  function onMonitor(aUsages) {
    var console = document.getElementById("system-monitor-multi-cpu-console");
    console.textContent = aUsages.map(function(aUsage) { return aUsage+'%'; }).join(' / ');

    CPUArray.shift();
    CPUArray.push(aUsages);

    var canvasElement = document.getElementById("system-monitor-multi-cpu");
    var context = canvasElement.getContext("2d")
    var y = canvasElement.height;
    var x = 0;

    context.fillStyle = "black";
    context.fillRect(0, 0, canvasElement.width, canvasElement.height);

    context.save();
    context.globalAlpha = 1 / aUsages.length;
    CPUArray.forEach(function(aUsages) {
      if (aUsages == undefined) {
        drawLine(context, "black", x, y, 0);
      } else {
        aUsages.forEach(function(aUsage) {
          drawLine(context, "lime", x, y, y - (y * aUsage));
        });
      }
      x = x + 2;
    }, this);
    context.globalAlpha = 1;
    context.restore();
  }

  function drawLine(aContext, aColor, aX, aBeginY, aEndY) {
    aContext.beginPath();
    aContext.strokeStyle = aColor;
    aContext.lineWidth = 1.0;
    aContext.lineCap = "square";
    aContext.moveTo(aX, aBeginY);
    aContext.lineTo(aX, aEndY);
    aContext.closePath();
    aContext.stroke();
  }

  // リスナを登録する。
  window.system.addMonitor("cpu-usages", onMonitor, interval);
}
// --></script>
```
{% endraw %}
