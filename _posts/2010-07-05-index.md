---
tags:
- ruby
title: ActiveLdap 1.2.2 - Rails 2.3.8対応
---
RubyらしいAPIでLDAPのエントリを操作できるライブラリ[ActiveLdap](http://ruby-activeldap.rubyforge.org/)の新しいバージョンがリリースされました。以下のようにgemでアップデートできます。
<!--more-->


{% raw %}
```
% sudo gem install activeldap
```
{% endraw %}

ActiveLdapについてはこのあたりを見てください。

  * [チュートリアル](http://code.google.com/p/ruby-activeldap/wiki/TutorialJa)
  * [Rubyist Magazine - ActiveLdap を使ってみよう（前編）](http://jp.rubyist.net/magazine/?0027-ActiveLdap)

今回のリリースではRuby on Railsの最新安定版2.3.8に対応しました。ActiveLdapは国際化対応のために[Ruby-GetText-Package](http://rubyforge.org/projects/gettext/)を使っています。そのため、ActiveLdapをRailsで使う場合に[locale_rails](http://rubyforge.org/projects/locale/)と一緒に使っている場合も多いでしょう。しかし、locale_railsの最新版はRails 2.3.8に対応していないので、locale_railsを利用している場合はアップデートするかどうかよく検討してください。（[locale_railsのリポジトリ](http://github.com/mutoh/locale_rails)上では2.3.8に対応しているので、locale_railsのリリース版ではなくて未リリースのものを利用するのも対応策の1つです。）

LDAPといえば、[日本Ruby会議2010](http://rubykaigi.org/2010/ja)では「Rubyで扱うLDAPのススメ」という企画があります。[[ANN]RubyKaigi2010 企画 "Ruby で扱う LDAP のススメ" にご協力頂ける方を募集しています - tashenの日記](http://d.hatena.ne.jp/tashen/20100620/1277054865)ということなので、ぜひ、ご協力をお願いします。
