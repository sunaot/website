---
tags:
- ruby
title: もっと知られてもいい人たち
---
[るりまサーチ](http://rurema.clear-code.com/)という最近の検索技術を使って[Rubyのリファレンスマニュアル](http://redmine.ruby-lang.org/wiki/rurema)を検索するWebアプリケーションがあります。表向きの存在理由は「手早く簡単にドキュメントを検索できるシステムを提供することで、Rubyユーザが楽しくプログラミングすることを妨げないようにする」ですが、実はもう一つ理由があります。それは、「Rubyのリファレンスマニュアルをよいものにしている人たちがいることに気づいてもらう」というものです。
<!--more-->


ここを読んでいる人の中に、他の人が実装したプログラミング言語やライブラリのドキュメントを書いて、メンテナンスしている（アップデートに追従するなど）人がどれだけいるのかわかりませんが、この作業はとても大変で根気のいる作業です。しかも、その成果をなかなか実感してもらえません。Ruby本体に新しい機能（例えば、「バイト長」ではなく「文字長」を数える機能）が入ったら、プログラムが簡潔になるなど、すぐに便利さを感じることができます。しかし、ドキュメントの方は「いやぁ、今読んだドキュメントはいいドキュメントだったなぁ！」と感じることはあまりないのではないでしょうか。

るりまサーチでRubyのリファレンスマニュアルを利用する人が増え、何度もその恩恵を受ければ徐々に便利さを感じてくるのではないでしょうか。そして、そう感じた人たちが[リファレンスマニュアルをよくするプロジェクトに参加](http://redmine.ruby-lang.org/wiki/rurema/HowToJoin)して、さらによくしていくことができたらよい循環になるのではないでしょうか。「Rubyを開発している人たちだけではなく、リファレンスマニュアルをよくしている人たちがいる」、るりまサーチがそのことを知ってもらうきっかけになれば作った甲斐があるというものです。

以下は第3回フクオカRuby大賞の本審査用に作った資料です。

[![るりまサーチ]({{ "/images/blog/20110203_0.png" | relative_url }} "るりまサーチ")](/archives/fukuoka-ruby-award-3/)
