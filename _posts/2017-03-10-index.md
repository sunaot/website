---
tags: []
title: MozillaのBugzillaへのパッチのsubmitのやり方
---
### はじめに

MozillaによるFirefoxやThuderbirdの開発は基本的にBugzillaのやり取りにより行われています。そのため、GitHubでのやり取りに比べとっつきにくい印象がどうしても拭えません。MozillaのBugzillaでの開発プロセスを一通り行ったのでまとめます。
[前回の記事]({% post_url 2017-02-22-index %}) は「macOS Printing」や「macOS printToFile PDF」などで検索して引っかかったBugにsubmitしたパッチを解説しました。
今回はそのパッチをsubmitしてmozilla-centralにコミットされるまでの流れを追います。
<!--more-->


### Bugzillaでのやり取り

Bugzillaでは以下の図のようなやり取りを通じて開発が進められます。[^0]

![review process]({{ "/images/blog/20170310_0.png" | relative_url }} "review process")

Mozillaの開発では基本的にパッチは何らかのBugに紐づけられるものです。
Bugを立てないことにはパッチのレビューをしてもらうことはできません。

#### 準備

まずは直面した問題がBugになっていないかを確認します。まずはキーワードにより検索します。該当のBugがあればこのBugの作業がしたいというコメントを残して権限のある人にAssignしてもらいます。

以降では[printToFile is busted on Mac | Mozilla Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=675709)でのやり取りを元に解説していきます。

#### パッチの作成

Bugにアサインされるといよいよパッチをsubmitするための準備が整います。
StatusはNEWのままで問題ないようです。

#### レビューに出す

MozillaのBugzillaでのレビューはreviewして欲しい人に対してreviewフラグを立てることにより始まります。また、レビューフラグを立ててもreviewフラグが立っている人が忙しいなどの理由で別の人にreviewフラグが移されることもあります。[printToFile is busted on Mac | Mozilla Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=675709)でのやり取りでは[Comment 19](https://bugzilla.mozilla.org/show_bug.cgi?id=675709#c19)のようにreviewフラグが別に人に移譲されています。

#### パッチのコミット

レビューをめでたく通過した場合は、mozillaのリポジトリにコミットしてもらう必要があります。通常はコミット権はないのでcheckin-neededキーワードをkeywordに追加します: https://bugzilla.mozilla.org/show_bug.cgi?id=675709#c19

このcheckin-neededキーワードが追加されていないとレビューには通っても永久にmozillaのリポジトリにコミットされないことに注意してください。

Mozillaの古いWikiではこの段階で既にmozilla-centralへのコミットがなされると書かれていることがありますが、現在は[mozilla-inbound](https://hg.mozilla.org/integration/mozilla-inbound)というリポジトリにまずはコミットされます。

[printToFile is busted on Mac | Mozilla Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=675709)でのやり取りでは一旦review+がもらえましたが、inboundでのビルド失敗により[差し戻されて](https://bugzilla.mozilla.org/show_bug.cgi?id=675709#c30)います。

執筆現在のinboundサーバーのmacOSのワーカーが10.7のため、未定義となってしまった定数が出てしまったようです。

[ステータスがFIXEDになればめでたく該当Bugでの作業は終了](https://bugzilla.mozilla.org/show_bug.cgi?id=675709#c33)です。

### まとめ

MozillaのBugzillaでの作業をどのように行うかを[printToFile is busted on Mac | Mozilla Bugzilla](https://bugzilla.mozilla.org/show_bug.cgi?id=675709)のBugを元に解説を試みました。GitHubによる開発プロセスになれた開発者にはとっつきにくい印象がありますが、GitHubができるよりも前から開発が続いているソフトウェアの開発プロセスを体験してみるのはいかがでしょうか。

[^0]: 以降は https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/How_to_Submit_a_Patch に基づいて解説します
