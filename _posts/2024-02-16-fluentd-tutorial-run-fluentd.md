---
title: Fluentdを動かしてみよう！
author: daipom
tags:
  - fluentd
---

こんにちは。[Fluentd](http://www.fluentd.org)のメンテナーの福田です。

Fluentdは、様々なデータソースからデータを読み込み、様々な出力先へ転送することができる便利なフリーソフトウェアです！

今回は、Fluentdに興味がある、触ってみたい、という方向けに、Fluentdを手元で動かす方法を紹介します。
RPMパッケージ、DEBパッケージ、MSI(Microsoft Windows Installer)を使ってパッケージ(Fluent Package)をインストールする方法と、ソースコードから起動する方法の2種類を説明します。

ユーザーとして機能を確かめてみたい、という方や、Fluentdやそのプラグインの開発に興味がある、という方は、ぜひご覧ください。

<!--more-->

## パッケージをインストールして動かす

Fluentdを便利に使うため、Fluentdプロジェクトはパッケージ「Fluent Package」を提供しています。
以前は「td-agent」という名前でした。
このパッケージをインストールすることで、すぐにFluentdを動かすことができます[^package]。

今回は次の3種類を紹介します。

* RPMパッケージ (Red Hat / CentOS / Rocky Linux / AlmaLinux)
* DEBパッケージ (Debian / Ubuntu)
* MSI (Windows)

もしmacOSで使いたいという人は、この後の「ソースコードから動かす」方法にするか、もしくは古いパッケージになりますが[Install by .dmg Package v4 (macOS)](https://docs.fluentd.org/installation/obsolete-installation/treasure-agent-v4-installation/install-by-dmg-td-agent-v4)を参考にしてください。

また、ダウンロードして利用できるものについては、次の公式サイトをご覧ください。
Dockerイメージなどもあります。

* https://www.fluentd.org/download

[^package]: パッケージには実行に必要なRubyなどが組み込まれているため、インストールするだけですぐに動かすことができます。GitやRuby環境を用意するのが大変、という方はパッケージで動かすのがおすすめです。一方で、Fluentdやそのプラグインの開発にも興味がある方は、「ソースコードから動かす」の方がおすすめです。

### RPMパッケージ (Red Hat / CentOS / Rocky Linux / AlmaLinux)

コマンド一発でインストールできます！

```console
$ curl -fsSL https://toolbelt.treasuredata.com/sh/install-redhat-fluent-package5-lts.sh | sh
```

インストールすると、systemdのユニット`fluentd`として登録されます。
次のように`systemctl`コマンドで確認できます。

```console
$ systemctl status fluentd
● fluentd.service - fluentd: All in one package of Fluentd
   Loaded: loaded (/usr/lib/systemd/system/fluentd.service; disabled; vendor preset: disabled)
   Active: inactive (dead)
     Docs: https://docs.fluentd.org/
```

早速起動してみましょう。

```console
$ sudo systemctl start fluentd
$ systemctl status fluentd
● fluentd.service - fluentd: All in one package of Fluentd
   Loaded: loaded (/usr/lib/systemd/system/fluentd.service; disabled; vendor preset: disabled)
   Active: active (running) since Thu 2024-02-01 14:35:25 UTC; 7s ago
     Docs: https://docs.fluentd.org/
  Process: ...
```

`running`状態になりました！

Fluent Packageでは、Fluentdの動作ログ[^fluentd-log]がデフォルトで`/var/log/fluent/fluentd.log`に出力されます。
ログ内容を確認してみましょう。

```console
$ cat /var/log/fluent/fluentd.log
2024-02-01 14:35:25 +0000 [info]: init supervisor logger path="/var/log/fluent/fluentd.log" rotate_age=nil rotate_size=nil
2024-02-01 14:35:25 +0000 [info]: parsing config file is succeeded path="/etc/fluent/fluentd.conf"
2024-02-01 14:35:25 +0000 [info]: gem 'fluentd' version '1.16.3'
...
```

何やら動いていそうですね！

次に設定を確認してみましょう。
Fluent Packageでは、設定ファイルはデフォルトで`/etc/fluent/fluentd.conf`になります。

```console
$ cat /etc/fluent/fluentd.conf
####
## Output descriptions:
...
```

何やら色々設定がありますね！

せっかくなので、もうちょっと遊んでみたいですよね。
それについてはインストール方法が関係ないので、最後の「設定を変更してデータを処理させる」で説明しています！
そちらをご覧ください！

またアンインストールは、`dnf`コマンドで`fluent-package`をアンインストールする形になります。

```console
$ sudo dnf remove fluent-package
```

参考: 公式ドキュメント https://docs.fluentd.org/installation/install-by-rpm

[^fluentd-log]: Fluentd自身の動作ログ: Fluentdがログを扱うソフトウェアなのでややこしいですが、Fluentd自身も動作ログを出力します。Fluentdが収集したり転送したりするログと、Fluentd自身の動作ログとを混同しないように気をつけてください！

### DEBパッケージ (Debian / Ubuntu)

コマンド一発でインストールできます！

ディストリビューションによってコマンドが異なります。

* `Ubuntu Jammy`
  ```console
  $ curl -fsSL https://toolbelt.treasuredata.com/sh/install-ubuntu-jammy-fluent-package5-lts.sh | sh
  ```
* `Ubuntu Focal`
  ```console
  $ curl -fsSL https://toolbelt.treasuredata.com/sh/install-ubuntu-focal-fluent-package5-lts.sh | sh
  ```
* `Debian Bookworm`
  ```console
  $ curl -fsSL https://toolbelt.treasuredata.com/sh/install-debian-bookworm-fluent-package5-lts.sh | sh
  ```
* `Debian Bullseye`
  ```console
  $ curl -fsSL https://toolbelt.treasuredata.com/sh/install-debian-bullseye-fluent-package5-lts.sh | sh
  ```

インストールすると、systemdのユニット`fluentd`として登録されます。
次のように`systemctl`コマンドで確認できます。

```console
$ systemctl status fluentd
systemctl status fluentd
● fluentd.service - fluentd: All in one package of Fluentd
     Loaded: loaded (/lib/systemd/system/fluentd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2024-02-01 15:01:46 UTC; 17s ago
       Docs: https://docs.fluentd.org/
    Process: ...
```

自動で動き始めていますね！
(今後のバージョンで変わるかもしれません。もし動いていなかったら`$ sudo systemctl start fluentd`コマンドを実行して起動してください。)

Fluent Packageでは、Fluentdの動作ログ[^fluentd-log]がデフォルトで`/var/log/fluent/fluentd.log`に出力されます。
ログ内容を確認してみましょう。

```console
$ cat /var/log/fluent/fluentd.log
2024-02-01 14:35:25 +0000 [info]: init supervisor logger path="/var/log/fluent/fluentd.log" rotate_age=nil rotate_size=nil
2024-02-01 14:35:25 +0000 [info]: parsing config file is succeeded path="/etc/fluent/fluentd.conf"
2024-02-01 14:35:25 +0000 [info]: gem 'fluentd' version '1.16.3'
...
```

何やら動いていそうですね！

次に設定を確認してみましょう。
Fluent Packageでは、設定ファイルはデフォルトで`/etc/fluent/fluentd.conf`になります。

```console
$ cat /etc/fluent/fluentd.conf
####
## Output descriptions:
...
```

何やら色々設定がありますね！

せっかくなので、もうちょっと遊んでみたいですよね。
それについてはインストール方法が関係ないので、最後の「設定を変更してデータを処理させる」で説明しています！
そちらをご覧ください！

またアンインストールは、`apt`コマンドで`fluent-package`をアンインストールする形になります。

```console
$ sudo apt purge fluent-package
```

参考: 公式ドキュメント https://docs.fluentd.org/installation/install-by-deb

### MSI (Microsoft Windows Installer)

[公式サイトの「Download Fluent Package」ページ](https://www.fluentd.org/download/fluent_package)に、[MSIファイルの一覧](https://td-agent-package-browser.herokuapp.com/lts/5/windows)へのリンクがあります。
ここからMSIファイルをダウンロードして、実行することでインストール可能です。
インストーラーでは、特に何も変更せずにデフォルトでインストールして問題ありません。

インストールすると、`fluentdwinsvc`という名前のWindows Serviceとして登録されます。
管理者権限のPowerShellを立ち上げて、確認してみましょう。

```powershell
$ Get-Service fluentdwinsvc

Status   Name               DisplayName
------   ----               -----------
Stopped  fluentdwinsvc      Fluentd Windows Service
```

早速起動してみましょう。

```powershell
$ Start-Service fluentdwinsvc
$ Get-Service fluentdwinsvc

Status   Name               DisplayName
------   ----               -----------
Running  fluentdwinsvc      Fluentd Windows Service
```

`Running`状態になりました！

Fluent Packageでは、Fluentdの動作ログ[^fluentd-log]がデフォルトで次のファイルに出力されます[^uui-log]。

* `C:\opt\fluent\fluentd-supervisor-0.log`
  * Supervisorプロセス(Fluentdの大元のプロセス)のログです
* `C:\opt\fluent\fluentd-0.log`
  * Workerプロセス(実際に作業を行う子分のプロセス)のログです

これらのファイルの中身を確認すると、何やらFluentdが動いていそうなことが分かります。

次に設定を確認してみましょう。
Fluent Packageでは、設定ファイルはデフォルトで`C:\opt\fluent\etc\fluent\fluentd.conf`になります。
このファイルの中身を確認すると、何やら色々設定があることが分かります。

せっかくなので、もうちょっと遊んでみたいですよね。
それについてはインストール方法が関係ないので、最後の「設定を変更してデータを処理させる」で説明しています！
そちらをご覧ください！

またアンインストールは、「設定」の「アプリと機能」や、「コントロールパネル」の「プログラムと機能」から、`Fluent Package v...`をアンインストールすることで可能です。
`C:\opt`のフォルダーも不要であれば消してください。

[^uui-log]: Windowsにおけるログファイル: Windowsではプロセス毎に別のログファイルに出力する形になります。

参考: 公式ドキュメント https://docs.fluentd.org/installation/install-by-msi

## ソースコードから動かす

FluentdはRubyで動いています。
そのため、GitとRubyの実行環境があれば、ソースコードから簡単に動かすことが可能です。

GitとRubyの実行環境がない場合は、事前に整えてください。

[GitHubのFluentdリポジトリー](https://github.com/fluent/fluentd)をクローンします。

```console
$ git clone https://github.com/fluent/fluentd.git
Cloning ...
...
```

クローンしたディレクトリーに移動します。

```console
$ cd fluentd
```

依存関係をダウンロードします。

```console
$ bundle
Fetching gem metadata from ...
...
```

以上で、Fluentdを動かす用意ができました。
いくつかやり方がありますが、ここでは`$ bundle exec fluentd ...`という形でFluentdを実行していきます。

まずはヘルプを見てみましょう。

```console
$ bundle exec fluentd --help
Usage: fluentd [options]
    -s, --setup [DIR=/etc/fluent]    install sample configuration file to the directory
    -c, --config PATH                config file path (default: /etc/fluent/fluent.conf)
...
```

`fluentd`コマンドのヘルプが表示されますね。

次にバージョンを確認してみましょう。

```console
$ bundle exec fluentd --version
fluentd 1.16.2
```

バージョンが表示されました。

次の章で、実際にFluentdの設定を用意してデータを処理させてみましょう。

参考: 公式ドキュメント https://docs.fluentd.org/installation/install-from-source

## 設定を変更してデータを処理させる

ここまで色々な方法でFluentdを動かす方法を見てきました。
最後に、実際にFluentdの設定をして、データを処理させてみましょう。

Fluentdは、「プラグイン」という機能を組み合わせることで様々な動作を実現できます。
このプラグインには、標準で搭載されているものもありますし、有志で作成されて公開されており、インストールすることで使えるものもあります。
Fluentdの設定の大半は、利用する各プラグインの設定になります。

今回は、[in_sampleプラグイン](https://docs.fluentd.org/input/sample)と[out_stdoutプラグイン](https://docs.fluentd.org/output/stdout)を使って、簡単にFluentdがデータを処理する様子を見てみましょう。

[in_sampleプラグイン](https://docs.fluentd.org/input/sample)は、[Inputプラグイン](https://docs.fluentd.org/input)[^input-plugin]の1種です。
通常のInputプラグインは、何かの外部データソースからデータを収集するものですが、`in_sample`は単にサンプルデータを1秒ごとにFluentdに流し込むだけです。
主にFluentdの動作チェックのために使います。
今回は簡単にFluentdの動作を確認するために、このInputプラグインを使ってみましょう。

[out_stdoutプラグイン](https://docs.fluentd.org/output/stdout)は、[Outputプラグイン](https://docs.fluentd.org/output)[^output-plugin]の1種です。
`out_stdout`は、標準出力にデータを出力します。
パッケージなどを利用してFluentd自身の動作ログをログファイルに出力している場合は、`out_stdout`も同じログファイルに出力することになります。
コマンド実行などで、Fluentd自身の動作ログをコンソールに出力している場合は、`out_stdout`もコンソールに出力することになります。
今回は、`in_sample`が流し込むサンプルデータを簡単に確認するため、このOutputプラグインを使ってみましょう。

設定は次の通りです。

```xml
<source>
  @type sample
  tag test
</source>

<match test.**>
  @type stdout
</match>
```

パッケージで動かしている場合は、既存の設定ファイルを上書きしてください。
設定が完了したら、Fluentdをリスタートしましょう。

```console
$ sudo systemctl restart fluentd
```

リスタート後、ログファイルを確認してみてください。
サンプルログが出力される様子を確認できます。

ソースコードから動かしている場合は、適当な場所に設定ファイルを新規作成してください。
設定ファイルを作成したら、`-c`オプションで設定ファイルのパスを指定してFluentdを起動しましょう。

```console
$ bundle exec fluentd -c {設定ファイルパス}
```

このようにログファイルパスを指定せずにコマンドでFluentdを起動すると、Fluentdはコンソールに自身の動作ログを出力します。
動作ログとともに、サンプルログが出力される様子を確認できます。
確認したら、`Ctrl` + `C`で停止します。

どちらの方法でも、次のように動作ログと一緒にサンプルログが出力されるのを確認できましたね！

```
2024-02-02 10:01:47 +0900 [info]: init supervisor logger path=nil rotate_age=nil rotate_size=nil
2024-02-02 10:01:47 +0900 [info]: parsing config file is succeeded path="../fluentd-conf/sample.conf"
2024-02-02 10:01:47 +0900 [info]: gem 'fluentd' version '1.16.2'
2024-02-02 10:01:47 +0900 [warn]: both of Plugin @id and path for <storage> are not specified. Using on-memory store.
2024-02-02 10:01:47 +0900 [warn]: both of Plugin @id and path for <storage> are not specified. Using on-memory store.
2024-02-02 10:01:47 +0900 [info]: using configuration file: <ROOT>
  <source>
    @type sample
    tag "test"
  </source>
  <match test.**>
    @type stdout
  </match>
</ROOT>
2024-02-02 10:01:47 +0900 [info]: starting fluentd-1.16.2 pid=618044 ruby="3.2.2"
2024-02-02 10:01:47 +0900 [info]: spawn command to main:  cmdline=["/home/daipom/.rbenv/versions/3.2.2/bin/ruby", "-r/home/daipom/.rbenv/versions/3.2.2/lib/ruby/site_ruby/3.2.0/bundler/setup", "-Eascii-8bit:ascii-8bit", "/home/daipom/.rbenv/versions/3.2.2/lib/ruby/gems/3.2.0/bin/fluentd", "-c", "../fluentd-conf/sample.conf", "--under-supervisor"]
2024-02-02 10:01:47 +0900 [info]: #0 init worker0 logger path=nil rotate_age=nil rotate_size=nil
2024-02-02 10:01:47 +0900 [info]: adding match pattern="test.**" type="stdout"
2024-02-02 10:01:47 +0900 [info]: adding source type="sample"
2024-02-02 10:01:47 +0900 [warn]: #0 both of Plugin @id and path for <storage> are not specified. Using on-memory store.
2024-02-02 10:01:47 +0900 [warn]: #0 both of Plugin @id and path for <storage> are not specified. Using on-memory store.
2024-02-02 10:01:47 +0900 [info]: #0 starting fluentd worker pid=618064 ppid=618044 worker=0
2024-02-02 10:01:47 +0900 [info]: #0 fluentd worker is now running worker=0
2024-02-02 10:01:48.048965556 +0900 test: {"message":"sample"}
2024-02-02 10:01:49.050802278 +0900 test: {"message":"sample"}
2024-02-02 10:01:50.052568659 +0900 test: {"message":"sample"}
...
```

この最後の部分が、`in_sample`が流し込んだサンプルデータを、`out_stdout`が出力したものになっています！

```
2024-02-02 10:01:48.048965556 +0900 test: {"message":"sample"}
2024-02-02 10:01:49.050802278 +0900 test: {"message":"sample"}
2024-02-02 10:01:50.052568659 +0900 test: {"message":"sample"}
...
```

[^input-plugin]: Inputプラグイン: あるデータソースからデータを収集する処理を行うプラグインです。慣習として`in_`というプレフィックスを付けて呼ぶことが多いです。`source`設定として設定します。
[^output-plugin]: Outputプラグイン: Inputプラグインが収集したデータなどを他のデータソースへ出力するプラグインです。慣習として`out_`といプレフィックスを付けて呼ぶことが多いです。`match`設定として設定します。

## まとめ

今回は、Fluentdを手元で動かしてみる方法について、RPMパッケージ、DEBパッケージ、MSI(Microsoft Windows Installer)を使ってパッケージ(Fluent Package)をインストールする方法と、ソースコードから起動する方法の2種類を説明しました。
Fluentdに興味がある方は、ぜひ実際に手元で動かしてみてください！

また、Fluentdコミュニティーでは、日本語用のQ&Aも用意しています。
何か疑問や困ったことがあれば、こちらで質問してみてください！

* https://github.com/fluent/fluentd/discussions/categories/q-a-japanese

最後に、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデート支援や影響のあるバグ・脆弱性のレポートなどのサポートをいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。
