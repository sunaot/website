---
tags:
  - apache-arrow
  - ruby
title: '名古屋Ruby会議03：Apache ArrowのRubyバインディング（2） #nagoyark03'
---
[前回はApache Arrowについて説明]({% post_url 2017-01-16-index %})しました。今回はなぜApache ArrowのRubyバインディングがあるとよさそうか説明します。ちなみに、「Apache ArrowのRubyバインディング」とは「Apache ArrowをRubyから使えるようにするライブラリー」のことです。「バインディング」については興味がある人は[RubyKaigi 2016：How to create bindings 2016]({% post_url 2016-09-14-index %})が参考になるはずです。
<!--more-->


Apache Arrowは複数のデータ分析プロダクトで共通で使われることを目指しているデータフォーマットの仕様（とその実装）です。Apache Arrowがうまくいくと書くデータ分析プロダクトはデータを交換するときにApache Arrowを使うということです。つまり、Apache Arrowを使えればその輪の中にまぜてもらえるということです。RubyでApache Arrowを使えればRubyもデータ分析の輪の中にまぜてもらえるということです。

Apache Arrowがまだ広く使われていない現状では、Rubyがデータ分析の輪の中に入るには各データ分析プロダクトと連携する部分を個別に用意していく必要があります。これは結構大変です。

Apache Arrowが広く使われている未来では、Apache Arrowに対応するだけで済みます。個別のプロダクトに対応する必要はありません。

このような理由からApache ArrowのRubyバインディングがあるとよさそうだと考えています。

次回はApache Arrowのバインディングを作るために利用しているGObject Introspectionについて説明します。
