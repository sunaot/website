---
title: Fluentd v1.15.0リリース -- YAML形式サポートなどの新機能とin_tailの不具合修正
author: daipom
tags:
  - fluentd
---

2022年6月29日にFluentdの最新版となるv1.15.0をリリースしました。

クリアコードは[Fluentd](http://www.fluentd.org)の開発に参加し、リリースを含めたメンテナンス作業を行っています。

今回はv1.15.0で追加された新機能を中心に紹介します。

* [CHANGELOG](https://github.com/fluent/fluentd/blob/master/CHANGELOG.md#release-v1150---20220629)
* [リリースアナウンス](https://www.fluentd.org/blog/fluentd-v1.15.0-has-been-released)

<!--more-->

## Fluentd v1.15.0の新機能

### YAML形式の設定をサポート

Fluentdの設定ファイルは、Apacheの`httpd.conf`の形式を模して、ディレクティブから成る形式を用いていました。

* [従来のFluentdの設定ファイルについて](https://docs.fluentd.org/configuration/config-file)
* [Apache HTTP サーバの設定ファイルについて](https://httpd.apache.org/docs/current/configuring.html)

一方で以前から、YAMLやJSONなどのより一般的な形式を使いたいという声も挙がっていました。

* [Issue#554](https://github.com/fluent/fluentd/issues/554)

本バージョンから、Kubernetesとの親和性を考慮して、YAML形式の設定ファイルも使用できるようになりました。詳細は公式ドキュメント[^fluentd-yamlconfig]の通りですが、ここではチュートリアル形式で主要なポイントをご紹介します。

[^fluentd-yamlconfig]: https://docs.fluentd.org/configuration/config-file-yaml

#### 1: シンプルな設定と実行

`in_sample`プラグイン[^in-sample]でサンプルデータを発生させ、`out_stdout`プラグインで標準出力に出力させるシンプルな設定です。

```yaml
config:
  - source:
      $type: sample
      tag: test.log
  - match:
      $type: stdout
      $tag: test.**
```

ポイントは次です。

* `config:`の配下に`source`などの各設定を並べます。
* `source`や`match`などの各項目は`- `をつけます[^yaml-list]。
* `source`や`match`の内部の各パラメータ設定は、インデントを1つ深くします[^indent-for-parameters]。
* `@type`の代わりに`$type`を用います。
* ディレクティブで指定していたタグは、`$tag`で指定します[^tag]。
    * `in_sample`の`tag`はプラグインの通常のパラメータなので`$`が付きません。

この設定を`fluent.yaml`のようなファイルに保存し、次のようにfluentdを起動することで使用できます。拡張子が`yaml`か`yml`であれば、Fluentdが自動でyaml形式と認識します。

```console
$ bundle exec fluentd -c fluent.yaml
```

[^in-sample]: https://docs.fluentd.org/input/sample
[^yaml-list]: これらはいくつでも並べられる項目であり、YAMLのリスト形式になります。
[^indent-for-parameters]: 間違いやすいポイントです。次のようにハッシュの二重構造になっています: `{"source": {"$type": "sample", "tag": "test,log"}}`
[^tag]: ディレクティブを用いない形式のため、 `<match test.**>` の `test.**` のように指定していたタグ等の値をパラメタとして指定する必要があります。同様のものに、後述する`$arg`や`$name`があります。

#### 2: labelの設定と注意するべき記号

`filter_record_transformer`プラグイン[^record-transformer]を使い、`{"label": "Test"}`というデータを付与した上で標準出力する処理を`@test`という名前のlabelとして設定します[^fluentd-label]。`in_sample`プラグインを1つ増やし、メッセージをカスタマイズした上でそのlabelに流します。

```yaml
config:
  - source:
      $type: sample
      tag: test.log
  - source:
      $type: sample
      $label: "@test"
      tag: test.log
      sample: "{\"message\": \"Sample for the test-label\"}"

  - match:
      $type: stdout
      $tag: test.**

  - label:
      $name: "@test"
      config:
        - filter:
            $type: record_transformer
            $tag: "**"
            record:
              label: Test
        - match:
            $type: stdout
            $tag: "**"
```

この設定は次のように、labelを流れないログとlabelに流れたログの双方を出力します。

```
{datetime} test.log: {"message":"sample"}
{datetime} test.log: {"message":"sample for the test-label","label":"Test"}
```

ポイントは次です。

* `@label`ではなくて`$label`を用います。
* `label`ディレクティブで指定していたラベル名は、`$name`で指定します。
* `label`の下に`config`を挟みます[^need-config]。
* 単語先頭の`@`や`*`はYAML形式の予約語なので、必ずクオーテーションで囲います。
* 従来の形式では`sample: {"message": "Sample for the test-label"}`のようにそのまま記載できましたが、YAMLでは全体をクオーテーションで囲い、内部のクオーテーションをエスケープします。

[^record-transformer]: https://docs.fluentd.org/filter/record_transformer
[^fluentd-label]: https://docs.fluentd.org/configuration/routing-examples#with-label-input-greater-than-filter-greater-than-output
[^need-config]: 後述する`worker`ディレクティブなど独立した設定の塊を定義するものは、このように再度`config`を挟み込みます。

#### 3: マルチワーカーの設定とRubyコードの埋め込み

`system`設定[^fluentd-systemconfig]を行い、4つのワーカーで動作させるようにします。さらに一部は特定のワーカーで動作するように指定します[^fluentd-multipleuuorkers]。またRubyコード`#{worker_id}`を埋め込み、動作したワーカーのidを出力データに追加します。

```yaml
system:
  workers: 4

config:
  - source:
      $type: sample
      tag: test.allworkers
      sample: "{\"message\": \"Run with all workers.\"}"

  - worker:
      $arg: 0
      config:
        - source:
            $type: sample
            tag: test.oneworker
            sample: "{\"message\": \"Run with only worker-0.\"}"

  - worker:
      $arg: 0-1
      config:
        - source:
            $type: sample
            tag: test.someworkers
            sample: "{\"message\": \"Run with worker-0 and worker-1.\"}"

  - filter:
      $type: record_transformer
      $tag: test.**
      record:
        worker_id: !fluent/s "#{worker_id}"

  - match:
      $type: stdout
      $tag: test.**
```

この設定で出力される次のログを見ると、想定したworkerでそれぞれの`in_sample`が動作していることが分かります。

```
{datetime} test.allworkers: {"message":"Run with all workers.","worker_id":"0"}
{datetime} test.allworkers: {"message":"Run with all workers.","worker_id":"1"}
{datetime} test.allworkers: {"message":"Run with all workers.","worker_id":"2"}
{datetime} test.allworkers: {"message":"Run with all workers.","worker_id":"3"}
{datetime} test.oneworker: {"message":"Run with only worker-0.","worker_id":"0"}
{datetime} test.someworkers: {"message":"Run with worker-0 and worker-1.","worker_id":"0"}
{datetime} test.someworkers: {"message":"Run with worker-0 and worker-1.","worker_id":"1"}
```

ポイントは次です。

* `system`設定は`config`とは別に設定します。
* `worker`ディレクティブで指定していたワーカーidは、`$arg`で指定します。
* Rubyコードの埋め込みを行うには、`!fluent/s`を付ける必要があります。

[^fluentd-systemconfig]: https://docs.fluentd.org/deployment/system-config
[^fluentd-multipleuuorkers]: https://docs.fluentd.org/deployment/multi-process-workers

#### YAML形式設定のまとめ

以上3例についてポイントを見てきました。

ここまで理解できれば、大抵の使い方はできるはずです。

従来の`@include`に相当する`!include`など、ここでは説明しきれない設定もありますので、より詳しい使い方については公式ドキュメント[^fluentd-yamlconfig]を参考にしてください。

### `in_tail`: 詳細な流量制限を可能にする`group`オプションを追加

`in_tail`プラグイン[^in-tail]に、新しいオプションとして`group`ディレクティブを追加しました。

`in_tail`プラグインはログファイルのデータ収集によく使われますが、一つのファイルでログが大量発生するとその処理にかかりっきりになり、他のファイルの処理が行われない点が問題でした。

この問題の対策として、`read_bytes_limit_per_second`オプション[^read-bytes-limit-per-second]をv1.13.0で追加し、流量を制限できるようになっていました。

しかし、これはユーザーにとって重要なログもそうでないログも一様に流量を制限してしまう、という問題が残っていました。

そこで今回、この`group`オプションを追加し、重要度に応じてファイル単位で細かな流量制限を行うことができるようになりました。

詳しい使い方は、次の公式ドキュメントをご覧ください。

* https://docs.fluentd.org/input/tail#less-than-group-greater-than-section

[^in-tail]: https://docs.fluentd.org/input/tail
[^read-bytes-limit-per-second]: https://docs.fluentd.org/input/tail#read_bytes_limit_per_second

### `system`: ワーカーの停止期間を設定する`restart_worker_interval`オプションを追加

`system`ディレクティブに、新しいオプション`restart_worker_interval`を追加しました。

ワーカープロセスがエラーや`kill`コマンド等で停止すると、Fluentdは即座にそのワーカープロセスをリスタートします[^restart-uuorker]。

しかし、意図的にワーカープロセスにしばらく停止してほしいというケースが、一部のユーザーから報告されていました。

* [Issue#3749](https://github.com/fluent/fluentd/issues/3749)

今回この新しいオプションを設定することで、設定した期間ワーカープロセスのリスタートを待機させることができます。

リスタートまで1分間待機させる設定例:

```
<system>
  restart_worker_interval 1m
</system>
```

`time`型の値を取るため、1時間なら`1h`のようにも設定できます。

[^restart-uuorker]: 厳密にはFluentdのSupervisorプロセスがワーカープロセスのリスタートを試みます。

### `fluent-ctl`: デバッグ用にFluentdの内部情報を出力させる`dump`コマンドを追加

`fluent-ctl`[^fluent-ctl]は、設定のリロードなどFluentdを操作するために使えるツールです。

用途に応じていくつかのコマンドを利用できますが、今回新しく`dump`コマンドを追加しました。

従来から、非Windows環境(UNIX系OSの環境)であれば、`SIGCONT`シグナルを対象プロセスに送信することで、Fluentdにデバッグ用の内部情報を出力させることができました[^dump-by-signal]。しかし、Windows環境ではシグナルを使うことができないため、内部情報を出力させる方法がありませんでした。

これを使うことで、Windows環境でもFluentdにデバッグ用の内部情報を出力させられるようになります。

次のように使うことができます。

```console
$ fluent-ctl dump [PID_OR_SVCNAME]
```

FluentdをWindows Serviceとして動作させている場合は、Service名を指定します。ただし、デフォルトのService名`fluentdwinsvc`のままであれば省略可能です。

Serviceとして動作させていない場合は、Supervisorプロセスのプロセスidを指定してください。

実行すると、システムの一時フォルダである`C:\\Windows\\Temp`配下にプロセス毎にファイルが出力されます。

詳しくは次の公式ドキュメントをご覧ください。

* https://docs.fluentd.org/deployment/command-line-option#about-dump

[^fluent-ctl]: https://docs.fluentd.org/deployment/command-line-option#fluent-ctl
[^dump-by-signal]: https://docs.fluentd.org/deployment/trouble-shooting#dump-fluentds-internal-information

### その他

`in_tail`関連の不具合を何点か修正しているので、より安定して使えるようになっています。

新しい機能を使う予定がなくても、アップグレードをお勧めします。

詳しくは、冒頭で紹介したリリースアナウンスページをご覧ください。

## まとめ

今回の記事では、Fluentd v1.15.0について最新情報をお届けしました。

最新版を使ってみて、何か気になる点があればぜひ[GitHub](https://github.com/fluent/fluentd/issues)で開発チームまでフィードバックをお寄せください！
