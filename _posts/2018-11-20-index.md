---
tags:
  - apache-arrow
  - ruby
  - presentation
title: 'RubyData Tokyo Meetup - Apache Arrow #RubyData_tokyo'
---
Apache ArrowのC・Ruby・パッケージ関連を主に開発している須藤です。
<!--more-->


[RubyData Tokyo Meetup](https://speee.connpass.com/event/105127/)でApache ArrowのRubyまわりの最新情報を紹介しました。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/rubydata-tokyo-meetup-2018/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/rubydata-tokyo-meetup-2018/" title="Apache Arrow">Apache Arrow</a>
  </div>
</div>


関連リンク：

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/rubydata-tokyo-meetup-2018/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/rubydatatokyomeetup2018apachearrow)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-rubydata-tokyo-meetup-2018)

### 内容

（いつ頃か忘れましたが）前にApache ArrowのRubyまわりを紹介した時はデータ交換まわりの話がメインでした。それは、データ交換まわりの実装しかなかったからです。

しかし、最近はデータ処理まわりの実装も進んできたので、そのあたりのことも盛り込みました。たとえば、素のRubyの機能で数値演算する場合と、`Numo::NArray`を使って数値演算する場合と、Gandiva（Apache Arrowの式処理モジュール）を使って数値演算する場合のコードとベンチマーク結果を紹介しました。

私のマシンで計測したところ`Numo::NArray`が一番高速でした。`Numo::NArray`すごい！発表中、[@sonots](https://twitter.com/sonots)さんがNumPyの方がさらに速いと思うけどねーと言いながら同じパターンをNumPyでも計測していました。計測したところ、NumPyよりも`Numo::NArray`の方が速く、[@naitoh](https://twitter.com/naitoh)さんも[その場で計測](https://gist.github.com/naitoh/08757e4bf5a6952c52f3add32370168c)したところ、確かに速かったです。この内容はその後の@naitohさんの[発表](http://naitoh.hatenablog.com/entry/2018/11/18/144659)に盛り込まれています。発表をきっかけに新たな事実の発見が進むなんていい集まりですね！

他には最近Apache Arrowで実装が進んでいるCSVパーサーが速いよ！ということを自慢したりしました。

### 集まりに関して

今回の集まりはとてもいい集まりだなぁと思えるいい集まりでした。

[@mrkn](https://twitter.com/mrkn)さんが[ポジティブな話](https://speakerdeck.com/mrkn/rubydata-current-and-future)をするようになっていたのもよかったですし、Juliaバックエンド案は面白いなぁと思いました。

[@shiro615](https://github.com/shiro615)さんの[OSS Gateワークショップ](https://oss-gate.github.io/workshop/report.html)でOSSの開発に参加しはじめて、[Red Data Tools](https://red-data-tools.github.io/ja/)で継続的にApache Arrowの開発に参加し続けて、この間コミッターになった、[という話](https://speakerdeck.com/shiro615/apache-arrowkomitutaninarumadefalsegui-ji)は感慨深かったです。OSS GateもRed Data Toolsもはじめてよかったな。

[@hatappi](https://twitter.com/hatappi)さんが[イベント中にRed ChainerのCumo対応ブランチをマージ](https://blog.hatappi.me/entry/2018/11/17/195938)していたのもよかったです。@sonotsさんの[発表](https://speakerdeck.com/sonots/introduction-of-cumo-and-integration-to-red-chainer)で変更の概要を聞いて、発表の後のコード懇親会で直接相談しながらマージ作業を進めていました。開発が進むなんて、なんていい集まりなんでしょう。

@sonotsさんはこのイベントがあったからCumo対応プルリクエストを作ったと言っていました。開発が進む集まり！

[@colspan](https://twitter.com/colspan)さんの[Menoh-RubyとFluentdを使って推論サーバーを作る話](https://www.slideshare.net/pfi/20181117-ruby-data-tokyo-meetup-menoh-rubypfnmiyoshi-123363185)は面白いなぁと思いました。なるほどなぁ。

Red Data ToolsとしてもMenohとMenoh-Rubyを応援していきたいので、いい感じに協力できないか少し相談しました。11月20日（火）の夜の[OSS Gate東京ミートアップ for Red Data Tools in Speee](https://speee.connpass.com/event/105237/)で続きを相談できそうです。

[@v0dro](https://twitter.com/v0dro)さんの発表で[XND](https://xnd.io/)関連の理解が深まりました。調べないとなぁと思っていたんですよねぇ。型を文字列で定義するのは、いいのかな、悪いのかな。まだ判断できないんですが、面白いアプローチだなぁとは思いました。

Red Data ToolsとしてもXND関連の開発に協力していきたいな。

### まとめ

2018年11月17日に[RubyData Tokyo Meetup](https://speee.connpass.com/event/105127/)という開発が進むいい集まりがありました。

Rubyでもっといい感じにデータ処理できるようになるといいなぁ思った人は次のアクションとして以下を検討してみてください。

  * 2018年11月20日（火）19:30開催の[OSS Gate東京ミートアップ for Red Data Tools in Speee](https://speee.connpass.com/event/105237/)に参加する

  * [Red Data Toolsのチャット](https://gitter.im/red-data-tools/ja)でなにから着手すればよさそうか相談する

  * 2018年12月8日（土）13:30開催の[Apache Arrow東京ミートアップ2018](https://speee.connpass.com/event/103514/)に参加する

  * 2018年12月11日（火）19:30開催の[OSS Gate東京ミートアップ for Red Data Tools in Speee](https://speee.connpass.com/event/105238/)に参加する
