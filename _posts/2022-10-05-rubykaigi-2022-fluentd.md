---
title: "RubyKaigi2022でRuby3とFluentdの講演をしてきました #rubykaigi2022"
author: fujimotos
tags:
  - ruby
  - fluentd
  - presentation
---

2022年9月8〜10日に開催されたRubyKaigi 2022に講演者として参加してきました。

* [RubyKaigi 2022 > How fast really is Ruby 3.x?](https://rubykaigi.org/2022/presentations/fujimotos.html#day2)
* [発表資料 (PDF)](https://raw.githubusercontent.com/fujimotos/RubyKaigi2022/master/20220909-RubyKaigi2022.pdf)

この記事では、カンファレンスに参加して印象に残った講演や出来事について書きます。

<!--more-->

### 現地で聞いた講演の感想

講演のフィードバックや感想を読むのは楽しいので、他の講演者さんの発表について、
現地で取ったメモからつらつらと書いていこうと思います。

#### <a href='https://rubykaigi.org/2022/presentations/yu_suke1994.html#day2'>Do Pure Ruby Dream of Encrypted Binary Protocol?</a>

* Rubyだとバイナリプロトコルの解析が難しい。Rubyには文字列と整数しかなく、バイナリ型がないのが大きなネックとなる。

* 比較として「Pythonには`bytes`がある[^python3] 」といった話が紹介されていた。このためPythonは処理速度の点でもRubyより速い。

* ではRubyでどうするか？ 方向性としては「ヘルパーとしてバイナリデータを処理するライブラリを実装する or C拡張で解決する」を検討しているとのこと。

[^python3]: 私の記憶だと、Pythonも2.xの時は（Rubyのように）文字列とバイト列の区別がなく、2→3の時に分離されたはず。
ただこの移行はむっちゃ大変だった。

#### <a href='https://rubykaigi.org/2022/presentations/m_seki.html#day2'>Create my own search engine</a>

* ポケモンカードの検索システムに関する発表。発表の比重としては「ポケモンカード:自然言語処理:Heroku」が8:1:1ぐらい。

* 基本的なアイデアとして、デッキを構成するカードを単語とみなし、自然言語検索を応用してデッキを検索できるようにしている。

* 工夫した点として、IDFによる類似度計算 [^idf] 、Herokuの無料枠で動かすときのコツ [^heroku] など。

[^idf]: この発表ではスパースなベクトルを工夫して計算していたが、numpy/pandasの発想からすると、
9000x20000のデータであれば、そのままメモリにのせて行列計算してしまう気がする。
Rubyだと難しいのかもしれない。

[^heroku]: ただHerokuの無料枠の提供は今年限りで終了とのこと。

#### <a href='https://rubykaigi.org/2022/presentations/rubylangorg.html#day2'>Ruby Committers vs The World</a>

* 記憶に残ったのは、Rubyコミッターの開発のモチベーションに関するやりとりだった。

* 新しい機能を開発するにあたって「具体的にこれこれに使いたいから開発している」という実利的な側面は（意外なことに？）割と希薄だったりする。

#### <a href='https://rubykaigi.org/2022/presentations/ioquatix.html#day3'>Real World Applications with the Ruby Fiber Scheduler</a>

* fiber/asyncの講演。もともとDNSサーバーを実装するためにasyncを書いたが、FalconというスケーラブルなHTTPサーバーも実現できて開発者としては大変満足とのこと。
* 途中で言及されたPuma vs Falconの比較は「Pumaはスレッドプールモデルなので、事前に用意したスレッド数が同時処理の上限になるが、FiberベースのFalconにはそのような制限がない」という話だと思う。

#### <a href='https://rubykaigi.org/2022/presentations/nay3.html#day3'>The Better RuboCop World to enjoy Ruby</a>

* RuboCopを機械的に運用すると弊害が生じる。RuboCopの指摘に対応するために、むしろダメな方向に書き直してしまう場合があるからだ。

* 講演を聞いていて、背後には「熟練したエンジニアのリソースは有限かつ希少であり、それをいかにスケールさせるか」という問題の構造がありそうだという印象を受けた。

* だから（コードレビューを肩代わりしてくれる）RuboCopは有用だが、あくまで単純な機械的ルールの集合なので、意図しない副作用も生むという話につながるのだと思う。

#### <a href='https://rubykaigi.org/2022/presentations/ima1zumi.html#day3'>String Meets Encoding</a>

* 印象に残った理由に、前日の講演がある。私の講演ではRubyをブラックボックスとして扱っていた。このため、どれくらい速くなったかということは言えたが、なぜ・どうして速くなったのかは説明ができなかった。

* この講演ではRubyの`String#Split`の実装を丁寧にプロファイルして、なぜ遅いのか？どうしたら改善ができるのか？を明らかにしていた。

* 講演者さんの話を聞きながら「Rubyの実装面まで掘り下げていればもう少し深い議論ができたかもしれない」と考えていた。

#### カンファレンス後のよもやま話

* カンファレンスが終わった後に、[TruffleRubyの開発者からコンタクトがあり](https://github.com/fujimotos/RubyKaigi2022/issues/1)「TruffleRubyはCRubyよりも速いし、互換性もバッチリでFluentdも動く」という話がありました。

* 会話の中で「来年のRubyKaigiでぜひ発表すべきだ！」と言ったら、「今年は参加できなかったけど、来年は挑戦するよ」と返事があったので、
もしかすると来年の長野・松本ではTruffleRubyの話が聞ける（かもしれません？）

### 今回の発表について

ついでに今回の自分の発表について、講演の背後にあった事柄について書き残しておきます。

#### 参考にした講演

* まず、今回の発表は、カーネル開発者のGreg Kroah-Hartmanの一連の講演に大きな影響を受けてます。
  とくに [Google TechTalks (2008)](https://www.youtube.com/watch?v=L2SED6sewRw) は大変参考にしました。

* 主に参考にしたのは、発表のスタイルの側面です。聞き手とフラットな立ち位置で話すこと、技術的なトピックに率直なコメントを加えること、途中で聞き手からのフィードバックを集めること、などなどです。

* 今回の発表で、この試みが少しでも成功していたら嬉しいです。

#### 今回の講演のゴール

* 今回の講演のゴールは、FluentdからRubyへのフィードバックを提供することでした。

* 私の好きな作家に [スヴェトラーナ・アレクシェーヴィチ](https://en.wikipedia.org/wiki/Svetlana_Alexievich) がいます。彼女は「多声的な作家」と言われます。多数の人々の声を集めることで、一つの世界を描き出すからです。

* 思うに、このテクニックはプログラミング言語にも応用できます。およそ「Rubyのパフォーマンス」と言っても、個々のRubyアプリによって見え方が異なっていて、それぞれに言うべきことがあるというのはいかにもありそうです。

* 言い換えると、Ruby3x3が実現されたかどうかは、幅広いRubyアプリの評価を集約して始めて分かるはずです。この議論は「現実のアプリからフィードバックを発信することが大事だ」という発想に繋がります。

この最後のポイントについては、他のRubyプロジェクトでもどんどん話ができるところだと思います。

### おわりに -- フィードバックを発信しよう！

前節の点について、例としてJekyllを取り上げてみます（なぜJekyllかというと、この記事をビルドするために今まさに使っているからです）。

Jekyllの処理を考えると、Markdownで書かれた大量のテキストをパースして、HTML形式にレンダリングします。
すると、JekyllもRubyのランタイムの中でガリガリと文字列を処理することになり、
Fluentdと似たパフォーマンス傾向を示すというのはいかにもありそうです。

実際、いくつかのRubyのバージョンでビルド時間を計測すると次のようになりました。

**Jekyllのビルド時間の比較 (clear-code.com)**

| #          | Jekyll | YJIT | TIME  | SPEED (Ruby2.5比) |
| ---------- | ------ | ---- | ----- | ----- |
| Ruby2.5    | v4.2.2 | なし   | 14.6s | **1.00x** |
| Ruby3.2dev | v4.2.2 | なし   | 13.6s | **1.07x** |
| Ruby3.2dev | v4.2.2 | あり   | 12.2s | **1.19x** |

Rubyのバージョンに追随してパフォーマンスが改善していること、
YJITが10%程度の速度向上をもたらしていることが分かります。

今回はFluentdの視点から発表を行いましたが、こういった形で色々なRubyアプリケーションからの声を聞けたらいいなと考えています。
