---
tags:
- ruby
title: Ruby-GNOME2 0.18.0リリース
---
[0.17.0のリリース]({% post_url 2008-09-08-index %})から1ヶ月も経っていませんが、[Ruby-GNOME2](http://ruby-gnome2.sourceforge.jp/ja/)の[バージョン0.18.0がリリース](http://blade.nagaokaut.ac.jp/cgi-bin/scat.rb/ruby/ruby-list/45522)されました。
<!--more-->


Ruby-GNOME2は[GTK+](https://ja.wikipedia.org/wiki/GTK%2B)を含む[GNOME](https://ja.wikipedia.org/wiki/GNOME)関連ライブラリの[Ruby](https://ja.wikipedia.org/wiki/Ruby)バインディング集です。

### 目玉

このリリースの目玉はメモリリークの修正と、新規バインディングの追加です。

メモリリークはRuby/GLibの中にもあり、Ruby-GNOME2関連ライブラリ全体で影響を受ける可能性が高いものでした。0.17.0を利用している場合は0.18.0に更新することをおすすめします。

新規バインディングとして以下の2つが追加されました。ただし、まだどちらも「実験的」マークがついていて、今後APIが変更される可能性があります。

  * Ruby/GtkSourceView2
  * Ruby/GooCanvas

#### Ruby/GtkSourceView2

Ruby/GtkSourceView2はソースコードハイライトウィジェットである[GtkSourceView](http://projects.gnome.org/gtksourceview/) 2.x系列をサポートします。以前のリリースにも含まれているRuby/GtkSourceViewはGtkSourceView 1.x系列をサポートしていて、2.x系列はサポートしていませんでした。

今回、別ライブラリになっているのはAPIに非互換性が発生したためです。それぞれのライブラリはrequireが異なります。Ruby/GtkSourceViewからRuby/GtkSourceView2へ移行する場合は以下のように変更する必要があります。

変更前:

{% raw %}
```ruby
require 'gtksourceview'
```
{% endraw %}

変更後:

{% raw %}
```ruby
require 'gtksourceview2'
```
{% endraw %}

GtkSourceView 1.x系列は開発が終了していて、現在は2.x系列が開発されています。今後のことを考えるとRuby/GtkSourceViewからRuby/GtkSourceView2へ移行を検討した方がよいのではないかと思います。

#### Ruby/GooCanvas

Ruby/GooCanvasは描画に[cairo](https://ja.wikipedia.org/wiki/cairo)を用いるキャンバスウィジェットである[GooCanvas](http://live.gnome.org/GooCanvas)のバインディングです。

キャンバスウィジェットとは図形や他のウィジェットなどを自由に配置できるウィジェットです。[Inkscape](https://ja.wikipedia.org/wiki/Inkscape)などのようなグラフィックツールを思い浮かべるとイメージしやすいかもしれません。Inkscapeでは丸や四角などの図形を好きな場所に配置することができます。キャンバスウィジェットを用いることで、そのような機能を持つアプリケーションを簡単に開発することができます。

現在、GTK+にはキャンバスウィジェットが含まれていませんが、将来のGTK+ではGooCanvasがGTK+のキャンバスウィジェットとして取り込まれるのではないかと予想しています。GTK+では[ProjectRidley/CanvasOverview - GNOME Live!](http://live.gnome.org/ProjectRidley/CanvasOverview)で検討しているようです。GooCanvasを含むいくつかのキャンバスウィジェットを比較しています。

ちなみに、Ruby-GNOME2にはRuby/GnomeCanvas2というGnomeCanvasのバインディングがあります。ただし、GnomeCanvasは非推奨ライブラリになっています。そのため、Ruby/GnomeCanvas2も将来的にRuby-GNOME2から削除される可能性があります。

これから新しくキャンバスウィジェットを用いたアプリケーションを開発する場合はRuby/GooCanvasも候補のひとつに入れた方がよいかもしれません。ただし、まだ「実験的」な段階なのでAPIが変更される可能性があることに注意する必要があります。

### 協力のお願い

アナウンスメールにもありますが、Ruby-GNOME2プロジェクトでは協力してくれる方を募集しています。例えば、バインディングを開発してくれる方、ドキュメントを書いてくれる方、英語のドキュメントを日本語化してくれる方、リリース作業をしてくれる方などを募集しています。

興味のある方は[ruby-gnome2-devel-ja ML](https://lists.sourceforge.net/lists/listinfo/ruby-gnome2-devel-ja)までお願いします。

0.17.0がリリースされてから何人かの方がドキュメント関連作業で協力してくれています。ありがとうございます！
