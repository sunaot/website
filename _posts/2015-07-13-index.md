---
tags:
- cutter
title: CutterのGStreamerサポートについて
---
### はじめに

C/C++に対応しているテスティングフレームワークの一つに[Cutter](http://cutter.sourceforge.net/index.html.ja)があります。
<!--more-->


今回はCutterのGStreamerサポートについて紹介します。Cutterの特徴について知りたい方は[Cutterの機能](http://cutter.sourceforge.net/reference/ja/features.html)を参照してください。
Cutterそのものについては過去に何度かククログでもとりあげていますので、そちらも併せて参照するとよいでしょう。

  * [Cutter導入事例: Senna (1)]({% post_url 2008-07-25-index %})

  * [Cutter導入事例: Senna (2)]({% post_url 2008-08-25-index %})

  * [C言語用単体テストフレームワークCutterへのHTTPテスト機能追加]({% post_url 2009-08-21-index %})

  * [SoupCutter で全文検索エンジンgroongaのHTTPインターフェースのテスト作成]({% post_url 2009-08-26-index %})

  * [C++用xUnitでのテストの書き方]({% post_url 2009-11-07-index %})

  * [CutterをWindowsでgrowlnotifyと一緒に使う方法]({% post_url 2012-12-12-index %})

  * [Cutterで画像を使ったテストを書くには]({% post_url 2014-09-30-index %})

### CutterのGStreamerサポートとは

CutterのGStreamerサポートと聞いて、何を思い浮かべるでしょうか。

  * CutterでGStreamerを使っているアプリケーションやライブラリのテストを書きやすくする仕組みのこと

  * GStreamerの仕組みを使ってテストを実行すること

前者のことと思ったかも知れません。正解は後者の「GStreamerの仕組みを使ってテストを実行すること」です。

### GStreamerを使ってテストを実行するには

では、実際に実行してみましょう。そのためには、この記事を書いている時点ではまだリリースされていないCutterの最新版を使う必要があります。GStreamer 1.0に対応したのがごく最近であるためです。

#### Cutterの最新版をビルドする

GitHubからソースコードを入手してビルドをはじめるために、次のコマンドを実行します。

```sh
% git clone https://github.com/clear-code/cutter.git
% cd cutter
% ./autogen.sh
% ./configure
```


ここで、 `configure` の結果で「GStreamer : yes」が含まれていることを確認します。ここで yes になっていないとGStreamerサポートが有効になりません。GStreamerの開発パッケージがインストール済であるか確認してください。
Debian系のディストリビューションでは、あらかじめ `libgstreamer1.0-dev` をインストールしておく必要があります。

```sh
Libraries:
  ...
  GStreamer                        : yes
  ...
```


うまくGStreamerを検出できていると、次のようにプラグインのディレクトリも正しいものが表示されます。

```sh
  GStreamer plugins directory      : /usr/lib/x86_64-linux-gnu/gstreamer-1.0
```


ここまできたら、あとは `make` を実行するだけです。

```sh
% make
```


#### テストを実行する

Cutterをビルドできたので、テストを実行してみましょう。 [^0]

cloneしたソースコードに `gst-plugins` というディレクトリがあるのでそちらに移動します。

```sh
% cd gst-plugins
```


すると、いくつかサンプルのシェルスクリプトがあるのがわかります。

```sh
% ls -la *.sh
-rwxr-xr-x 1 kenhys kenhys 584  7月 11 01:52 run-client.sh
-rwxr-xr-x 1 kenhys kenhys 580  7月 11 01:52 run-server.sh
-rwxr-xr-x 1 kenhys kenhys 679  7月 11 01:52 run-test.sh
```


ここでは、一番簡単な `run-test.sh` の内容を見てみましょう。

```sh
% cat run-test.sh
#!/bin/sh

export BASE_DIR="`dirname $0`"

if test x"$NO_MAKE" != x"yes"; then
    make -C $BASE_DIR/../ > /dev/null || exit 1
fi

export CUT_UI_MODULE_DIR=$BASE_DIR/../module/ui/.libs
export CUT_UI_FACTORY_MODULE_DIR=$BASE_DIR/../module/ui/.libs
export CUT_REPORT_MODULE_DIR=$BASE_DIR/../module/report/.libs
export CUT_REPORT_FACTORY_MODULE_DIR=$BASE_DIR/../module/report/.libs
export CUT_STREAM_MODULE_DIR=$BASE_DIR/../module/stream/.libs
export CUT_STREAM_FACTORY_MODULE_DIR=$BASE_DIR/../module/stream/.libs

export GST_PLUGIN_PATH=$BASE_DIR/.libs
gst-launch-1.0 \
  cutter-test-runner test-directory=$BASE_DIR/test ! \
  cutter-console-output verbose-level=v use-color=true
```


ポイントは最後の行で `gst-launch-1.0` を使っているところです。

```sh
gst-launch-1.0 \
  cutter-test-runner test-directory=$BASE_DIR/test ! \
  cutter-console-output verbose-level=v use-color=true
```


Cutter特有のエレメントである、「`cutter-test-runner`」や、「`cutter-console-output`」を指定してパイプラインを組み立てているのがわかります。 [^1]

では、`run-test.sh` を実行してみましょう。

```sh
% ./run-test.sh
パイプラインを一時停止 (PAUSED) にしています...
Pipeline is PREROLLING ...
Pipeline is PREROLLED ...
パイプラインを再生中 (PLAYING) にしています...
New clock: GstSystemClock
dummy_loader_test:
  test_dummy_function2:                                 .: (0.000001)
  test_dummy_function3:                                 .: (0.000001)
  test_dummy_function1:                                 .: (0.000000)
  test_abcdefghijklmnopqratuvwzyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789:.: (0.000000)

Finished in 0.000935 seconds (total: 0.000002 seconds)

4 test(s), 0 assertion(s), 0 failure(s), 0 error(s), 0 pending(s), 0 omission(s), 0 notification(s)
100% passed
Got EOS from element "pipeline0".
Execution ended after 0:00:00.000993224
パイプラインを一時停止 (PAUSED) にしています...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
```


GStreamerでよくみるメッセージとともに、テストの実行結果が表示されているのがわかります。

この例ではテストを実行するのも、結果を表示するのも同一PCで行っています。

### テストの実行と結果を別の環境で行う

先程の例では、テストの実行と結果を表示するのは同一のPCにて行っていました。今度はそれを、別の環境で行ってみましょう。
GStreamerに付属の `tcpserversrc` や `tcpclientsink` といったエレメントを使って実現できます。

テストを実行する側では `tcpclientsink` を指定し、テスト結果を受けとって表示する側では `tcpserversrc` を指定して TCPによるテスト結果の送受信をするのがポイントです。

`gst-launch-1.0` のパイプライン指定はそれぞれ次のようになります。

テスト実施側のコマンドライン:

```sh
% gst-launch-1.0 \
    cutter-test-runner test-directory=$BASE_DIR/test ! \
    tcpclientsink host=(テスト結果受信側のIP) port=50000
```


テスト結果の受信側のコマンドライン:

```sh
% gst-launch-1.0 \
    tcpserversrc host=(テスト結果受信側のIP) port=50000 ! \
    cutter-console-output verbose-level=v use-color=true
```


簡単に実行するために、テスト実施側用として `run-server.sh` があります。また、テスト結果の受信側用に `run-client.sh` があります。

それぞれのスクリプトを実行する順番は次の通りです。

  * `run-client.sh` をテスト結果受信側で実行する

  * `run-server.sh` をテスト実施側で実行する

```sh
$ ./run-client.sh 
Setting pipeline to PAUSED ...
Pipeline is PREROLLING ...
Pipeline is PREROLLED ...
Setting pipeline to PLAYING ...
New clock: GstSystemClock
dummy_loader_test:
  test_dummy_function2:                                 .: (0.000000)
  test_dummy_function3:                                 .: (0.000000)
  test_dummy_function1:                                 .: (0.000001)
  test_abcdefghijklmnopqratuvwzyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789:.: (0.000001)

Finished in 0.003666 seconds (total: 0.000002 seconds)

4 test(s), 0 assertion(s), 0 failure(s), 0 error(s), 0 pending(s), 0 omission(s), 0 notification(s)
100% passed
Got EOS from element "pipeline0".
Execution ended after 0:00:00.005115664
Setting pipeline to PAUSED ...
Setting pipeline to READY ...
Setting pipeline to NULL ...
Freeing pipeline ...
```


これで、テストの実行環境とは別の環境で結果を表示することができました。
リモートで実行中のCutterのテスト結果のログを手元に保存する、というのが簡単にできるようになります。

### まとめ

今回はCutterというテスティングフレームワークのGStreamerサポートについて紹介しました。

Cutterにはほかにもテスト環境を便利にする機能があります。まだCutterを使ったことがない人は[チュートリアル](http://cutter.sourceforge.net/reference/ja/tutorial.html)からはじめるとよいでしょう。詳しく知りたい人は[リファレンスマニュアル](http://cutter.sourceforge.net/reference/ja/)を参照してください。

[^0]: 付属のサンプルを実行するだけなので、make installは実行していない。

[^1]: エレメントの詳細は gst-inspect-1.0 で確認できる。
