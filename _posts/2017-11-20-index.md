---
tags:
- fluentd
title: Fluentd v0.14.xが安定版になりました
---
* [Fluentd v0.14.22 has been released | Fluentd](https://www.fluentd.org/blog/fluentd-v0.14.22-has-been-released)
<!--more-->


少し前の話ですが、2017年11月01日にFluentd v0.14.22 がリリースされました。
このリリースでは以下のようにstableなリリースだと宣言されました。

> v0.14.22 is a stable release of v0.14 series


安定版になったので安定版を待っていたプラグイン開発者の人は、諸々対応していただけると大変ありがたいです :pray:

このリリースが出てからは、主にドキュメントを充実させるべく[docs.fluentd.org](https://docs.fluentd.org)の[リポジトリ](https://github.com/fluent/fluentd-docs)にPull requestを送っています。

11月20日時点で約40件PullRequestを送って[plugin helper](https://docs.fluentd.org/v0.14/categories/plugin-helpers)と[sections](https://docs.fluentd.org/v0.14/categories/plugin-helper-sections) (buffer sectionは既に書かれていた)のドキュメントを充実させました。
これで、これまではソースコードを読まないとわからなかったplugin helperの使い方や、plugin helperを使ったプラグインの設定の書き方をまとめることができました。

記事執筆時点でざっと見渡したところ、不足しているドキュメントは以下の通りでした。

  * Writing Buffer Plugins: 全体

    * 必要としている人は少ないのであとまわし

  * Writing Storage Plugins: 全体

    * 必要としている人は少ないのであとまわし

  * Output Plugin Overview: secondary output の例を追加

  * その他、不足している部分の改善や追加

    * built-in プラグインのドキュメント

### まとめ

Fluentd v0.14 のplugin APIとplugin helperを使ったプラグインを書くためには、ソースコードを読む必要がありましたが、ドキュメントを整備したのでドキュメントを読めばFluentd v0.14のplugin APIやplugin helpersを使って少ない記述量で高度なプラグインを開発できるようになりました。

built-inプラグインのドキュメントについても今後、書いていく予定です。
