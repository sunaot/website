---
title: PostgreSQL Conference Japan 2021　招待チケットプレゼント！
author: yoshimotoy
tags:
- groonga
---

クリアコードで、コードを書く以外の仕事をしてる吉本です。

クリアコードは、[PostgreSQL Conference Japan 2021](https://www.postgresql.jp/jpug-pgcon2021)をシルバースポンサーとして応援しています。

明日（11月11日9時30分）までの応募で、1名様に招待チケットをプレゼントします！

<!--more-->

11月12日開催されるPostgreSQL Conference Japan 2021は、参加に有償チケットが必要です。
通常のチケットと、有料のチュートリアルセッション参加可能のチケット（8000円相当）があります。チュートリアルセッション分のチケット好評につきすでに完売しています。

クリアコードは、スポンサーをしているので、スポンサー特典の招待チケットをもっています。関係者で使いきれなかった分を参加したい人にプレゼントします。


いろいろと時間が限られていますが、講演を聞きたい方はぜひご応募ください！詳細は次の通りです。


## チケットプレゼント詳細

* 定員：1名
* 応募資格：
  * 2021-11-12（金）に行われるPostgreSQL Conference Japan 2021（東京・AP日本橋）に参加できる人
  * Twitterの@_clear_codeからのダイレクトメッセージで当選の通知を受け取れる方
* 応募方法：
  * Twitterで@_clear_codeをフォロー
  * 2021-11-11（木）9:30:00 JSTまでにTwitterで@_clear_codeへ「#PostgreSQL Conference Japan 2021に参加したい！」 からはじめて思いの丈を綴った上でツイートしてください。例：「@_clear_code #PostgreSQL Conference Japan 2021に参加したい！PGroongaをつかっているけれどもっと使いこなす方法を知りたい！」 
* 選考：
  * 当選者は@_clear_codeおよびこのブログで発表
  * 2021-11-11（木）に当選者にTwitterの@_clear_codeからダイレクトメッセージで詳細を通知
  * 自分のブログなどにPostgreSQL Conference Japan 2021の参加レポートを書いてくれる方を優先
  * PGroongaを実際に使っていて、[ユーザーとして紹介](https://pgroonga.github.io/ja/users/)しても大丈夫な方を優先
* 注意事項:
  * 当選した方へは、事前に情報登録のためお名前などをお伺いします。スムーズにご返答がいただけない場合、参加が難しくなる場合があります。了承のうえ、応募ください。
