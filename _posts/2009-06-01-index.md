---
tags:
- mozilla
title: Windows Mobile用Fennecのビルド方法
---
2009年6月1日時点でのWindows Vista上でWindows Mobile用Fennecをビルドする方法を紹介します。時間が経つとビルド方法が変わると思うので、注意してください。
<!--more-->


### 情報源

Fennecのビルド方法に関する情報は[Mobile/Build/Fennec - MozillaWiki](https://wiki.mozilla.org/Mobile/Build/Fennec)（英語）にあります。最新情報が必要な場合はこのWikiを見てください。

ただし、Wiki上の情報が古いことがあるので注意が必要です。

### 下準備

Fennecをビルドするために必要なツールをインストールします。
（参考: [Mobile/Build/Windows Mobile PrepForBuild - MozillaWiki](https://wiki.mozilla.org/Mobile/Build/Windows_Mobile_PrepForBuild)）

まず、Microsoftが提供しているツールをインストールします。

  * [Windows SDK for Windows Vista](http://www.microsoft.com/downloads/details.aspx?FamilyID=7614fe22-8a64-4dfb-aa0c-db53035f40a0&DisplayLang=ja)
  * Visual Studio 2008（Express Editionは不可）
  * [Windows Mobile 6 Professional SDK](http://www.microsoft.com/downloads/details.aspx?familyid=06111A3A-A651-4745-88EF-3D48091A390B&displaylang=en)
  * [Windows Mobile 6.1 Localized Emulator Images](http://www.microsoft.com/downloads/details.aspx?familyid=3D6F581E-C093-4B15-AB0C-A2CE5BFFDB47&displaylang=en)（エミュレータのイメージはWindows Mobile 6.0ではなく、6.1を使う）

次に、Mozillaが提供しているビルドツールをインストールします。

  * [Windows Mozilla Build Setup](http://ftp.mozilla.org/pub/mozilla.org/mozilla/libraries/win32/MozillaBuildSetup-Latest.exe)

このビルドツールの中にはMSYSやMercurialなどのツールが入っています。

「c:\mozilla-build\start-msvc9.bat」というバッチファイルがインストールされます。このバッチファイルを実行することにより、bashが起動し、Fennecビルド用環境に入ることができます。

### ビルド

ビルドに必要なツールが揃ったので、それらのツールを使ってFennecをビルドする方法を説明します。（参考: [Mobile/Build/Windows Mobile BuildingIt - MozillaWiki](https://wiki.mozilla.org/Mobile/Build/Windows_Mobile_BuildingIt)）

Fennecのソースコードは[Mercurial](https://ja.wikipedia.org/wiki/Mercurial)で管理されています。まず、Mercurialでソースコードを取得します。ここからはstart-msvc9.batで起動した環境の中で作業します。

ソースコードは「c:\hg\」以下に置くことにします。

{% raw %}
```
$ mkdir /c/hg/
$ cd /c/hg/
$ hg clone http://hg.mozilla.org/mozilla-central
$ cd mozilla-central
$ hg clone http://hg.mozilla.org/mobile-browser mobile
```
{% endraw %}

ソースコードを取得したらビルド用の設定ファイル「.mozconfig」を作成します。.mozconfigは雛形となるファイルを少しずつ変更して作成するとよいでしょう。ここでは、クリアコードが公開している雛形を利用します。

{% raw %}
```
$ wget -O .mozconfig http://www.clear-code.com/repos/svn/fennec/wince.mozconfig
```
{% endraw %}

現在、リポジトリ内にデフォルトの.mozconfigを入れてはどうか、という話がでている（[Bug 479515 – push default mozconfig to mobile-browser repo](https://bugzilla.mozilla.org/show_bug.cgi?id=479515)）ので、将来的には取得したソースコード中から雛形を利用できるようになることでしょう。

.mozconfigを作成したら、以下のコマンドでビルドできます。

{% raw %}
```
$ time make -f client.mk build
```
{% endraw %}

マシンの速度にもよりますが、1時間以上かかるでしょう。timeはビルド時間を計測するためにつけているだけなので、以下のようにtime無しでビルドすることもできます。

{% raw %}
```
$ make -f client.mk build
```
{% endraw %}

ビルドの完了を待つ間に、最新版に追従する方法を説明します。

### ソースコードのアップデート

Mozillaのソースコードは毎日アップデートされています。最新版のソースコードに追従するためには、ソースコードがあるディレクトリで以下のコマンドを実行します。

{% raw %}
```
$ hg pull && hg update && (cd mobile && hg pull && hg update)
```
{% endraw %}

このコマンドを実行することにより、手元のソースコードが最新版になります。

アップデートされたソースコードでビルドしなおす場合も最初のビルドのときと同じコマンドを使います。

{% raw %}
```
$ time make -f client.mk build
```
{% endraw %}

マシンの速度やソースコードがどの程度アップデートされたかにもよりますが、30分以上かかるでしょう。ビルドの完了を待つ間に、Fennecのデバッグ方法を説明します。

### デバッグ

Fennecのデバッグ方法を説明します。（参考: [Mobile/Build/Windows Mobile DebuggingIt - MozillaWiki](https://wiki.mozilla.org/Mobile/Build/Windows_Mobile_DebuggingIt)）

まず、Visual StudioでFennecデバッグ用のプロジェクトを作成します。プロジェクトの種類は「Visual C++→スマートデバイス→Win32スマートデバイスプロジェクト」を選んでください。このプロジェクトはデバッガを使うためだけのプロジェクトなので設定はデフォルト値で問題ありません。

プロジェクトを作成したらプロジェクトのプロパティで以下を設定します。

  * 構成プロパティ→デバッグ→リモート実行ファイル:
    「\Storage Card\fennec.exe」
  * 構成プロパティ→配置→配置デバイス:
    「USA Windows Mobile 6.1.4 Professional VGA Emulator」

設定したらF5でデバッグを開始します。エミュレータが起動するので、Fennecのビルド結果を出力したディレクトリをエミュレータの「ストレージカード」としてアクセスするための設定をします。この設定は1回行えば今後はその設定を利用できます。

「ストレージカード」としてアクセスするためには、エミュレータのウィンドウメニューから「ファイル→構成→全般→共有フォルダ」とアクセスします。「共有フォルダ」に「c:\hg\fennec-debug\mobile\dist\bin」を指定してください。

これで、エミュレータ上で「\Storage Card\fennec.exe」としてFennecを実行できるようになります。もう一度、Visual Studio上でF5を押してデバッグを開始してください。デバッガ上でFennecが起動します。

注: 現在のFennecは初回起動時（プロファイルがない状態で起動した場合）のみ内部で再起動しています。そのため、再起動した段階でデバッガからデタッチしてしまいます。2回目以降の起動ではそのままデバッガ上で動くので、一度タスクマネージャからFennecを終了してF5でデバッグを開始しなおしてください。

### エミュレータでのネットワーク接続

エミュレータでネットワークに接続するためには[Virtual PC](http://www.microsoft.com/downloads/details.aspx?displaylang=ja&FamilyID=28c97d22-6eb8-4a09-a7f7-f6c7a1f000b5)をインストールする必要があります。

Virtual PCをインストールするとエミュレータでネットワークを利用できるようになります。エミュレータのウィンドウメニューから「ファイル→構成→NE2000 PCMCIAネットワークアダプタを有効にし、次の項目にバインドする」とアクセスします。この項目にチェックを入れるとエミュレータからネットワークにアクセスできるようになるので、エミュレータ上のFennecからWebを閲覧することができるようになります。

Windows Mobile 6.1.4用のエミュレータは英語用のため日本語フォントが入っていません。日本語を表示するためには、[Fennecでの日本語表示設定]({% post_url 2009-04-28-index %})のように別途日本語フォントを入れる必要があります。

### リリース用ビルドの作成

エミュレータ上でのFennecのデバッグについて説明したので、次に、実機へFennecをインストールする方法を説明します。

雛形.mozconfigではデバッグ用ビルドの設定が有効になっています。しかし、実機へインストールする場合は最適化されたリリース用ビルドの方がよいでしょう。インストール方法を説明する前に、まず、リリース用ビルドの設定を説明します。

雛形.mozconfigの中に以下のような記述があります。

{% raw %}
```
ac_add_options --enable-debug
ac_add_options --disable-optimize
mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/../fennec-debug
#ac_add_options --enable-optimize
#mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/../fennec-release
```
{% endraw %}

この部分のコメントアウトされていない部分がデバッグ用ビルドの設定で、コメントアウトされている部分がリリース用ビルドの設定です。つまり、リリース用ビルドにする場合は以下のように変更します。

{% raw %}
```
#ac_add_options --enable-debug
#ac_add_options --disable-optimize
#mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/../fennec-debug
ac_add_options --enable-optimize
mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/../fennec-release
```
{% endraw %}

この.mozconfigを使ってビルドすることにより最適化されたリリース用ビルドを作成することができます。

それでは、リリース用ビルドを使ってWindows Mobile用のインストーラを作成する方法を説明します。

### Windows Mobile用のインストーラの作成

Windows Mobile用のインストーラである.cabファイルを作成するには、「$(MOZ_OBJDIR)/mobile/」ディレクトリ（$(MOZ_OBJDIR)は.mozconfigで指定したビルド結果出力ディレクト
リ）で以下のコマンドを実行します。

{% raw %}
```
$ make installer
```
{% endraw %}

雛形.mozconfigのように

{% raw %}
```
mk_add_options MOZ_OBJDIR=@TOPSRCDIR@/../fennec-release
```
{% endraw %}

としていた場合は、以下のようになります。

{% raw %}
```
$ cd ../fennec-release/mobile
$ make installer
```
{% endraw %}

コマンドが完了すると「$(MOZ_OBJDIR)/mobile/dist/」以下にfennec.1.0a1.en-US.wince-arm.cabファイルができます。（ファイル名の中の「1.0a1」はバージョン番号でバージョン毎に異なります。）このファイルをActiveSyncなどで実機へコピーしてインストールすることができます。

### まとめ

Windows Vista上でFennecのビルド・デバッグ・インストーラ作成を行う方法を紹介しました。

これで環境環境は用意できると思うので、あとは「開発合宿後も、Fennec の開発活動に関わる意思を有する方」という条件を満たせば、[Mozilla Japan主催のMobile Firefox開発合宿](http://mozilla.jp/blog/entry/4108/)に参加できます。興味のある方は参加してはいかがでしょうか。
