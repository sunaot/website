---
tags:
  - apache-arrow
  - presentation
title: 'SciPy Japan 2020 - Apache Arrow 1.0 - A cross-language development platform
  for in-memory data #SciPyJapan #ApacheArrow'
---
[SciPy Japan 2020](https://www.scipyjapan.scipy.org/)の1日目に「Apache Arrow 1.0 - A cross-language development platform for in-memory data」という話をする須藤です。
<!--more-->


SciPy Japan 2020は2020年10月30日で約1ヶ月後なのですが、事前録画した動画を使ったオンラインイベントで、私の話は録画済みなのでここで先に公開します。SciPy Japan 2020に参加する人とこのブログを読む人は違う層だろうからです。Apache Arrowをよく知らない人がどうやってApache Arrowを使えばよいかがわかることを狙った内容にしました。Apache Arrowをよく知らない人に観てもらって（動画ではなくてスライドでもよいです）感想を https://twitter.com/ktou に教えて欲しいです！

<div class="youtube">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/tYO8XpXiG4A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2020/viewer.html"
          width="640" height="404"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2020/" title="Apache Arrow 1.0 - A cross-language development platform for in-memory data">Apache Arrow 1.0 - A cross-language development platform for in-memory data</a>
  </div>
</div>


関連リンク：

  * [動画（YouTube）](https://www.youtube.com/watch?v=tYO8XpXiG4A)

  * [スライド（Rabbit Slide Show）](https://slide.rabbit-shocker.org/authors/kou/scipy-japan-2020/)

  * [スライド（SlideShare）](https://www.slideshare.net/kou/scipy-japan-2020)

  * [リポジトリー](https://gitlab.com/ktou/rabbit-slide-kou-scipy-japan-2020)

### 内容

Apache Arrowに関する情報は毎年まとめている次の情報で網羅的にカバーできているはずですが、多岐にわたっていてApache Arrowを調べ始めた人には情報量が多すぎるんじゃないかと感じていました。

  * [Apache Arrowの最新情報（2020年7月版）]({% post_url 2020-07-31-index %})

  * [Apache Arrowの最新情報（2019年9月版）]({% post_url 2019-09-30-index %})

  * [Apache Arrowの最新情報（2018年9月版）]({% post_url 2018-09-05-index %})

これまでいろいろな機会でApache Arrowを紹介してきましたが「すごいよさそうですね！で、どう使い始めればよいですか！？」という反応をもらうことが多かったです。

そこで、今回はApache Arrowをよく知らない人が知りたそうだろうことにフォーカスして情報を削ってまとめてみました。ただ、私はApache Arrowをよく知らない人ではないのでうまくいっているかどうかよくわかりません。ということで、Apache Arrowをよく知らない人の感想を聞きたいのです！ぜひ https://twitter.com/ktou に感想を教えてください！
