---
title: 'PostgreSQL Conference Japan 2021：PGroongaを使って全文検索結果をより良くする方法
  #pgcon21j'
author: komainu8
tags:
- groonga
- presentation

---
[PGroonga](https://pgroonga.github.io/ja/)の[サポートサービス]({% link services/groonga.md %})を担当している堀本です。

2021年11月12日(金)に[PostgreSQL Conference Japan 2021](https://www.postgresql.jp/jpug-pgcon2021)が開催されます。
私は、「PGroongaを使って全文検索結果をより良くする方法」という題名で、PGroongaでより良い検索結果を得るのに使える機能を紹介します。

<!--more-->


当日使用する資料は、以下に公開しています。

<div class="rabbit-slide">
  <iframe src="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2021/viewer.html"
          width="640" height="524"
          frameborder="0"
          marginwidth="0"
          marginheight="0"
          scrolling="no"
          style="border: 1px solid #ccc; border-width: 1px 1px 0; margin-bottom: 5px"
          allowfullscreen> </iframe>
  <div style="margin-bottom: 5px">
    <a href="https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2021/" title="PGroongaを使って全文検索結果をより良くする方法">PGroongaを使って全文検索結果をより良くする方法</a>
  </div>
</div>


関連リンク：

  * [スライド(Rabbit Slide Show)](https://slide.rabbit-shocker.org/authors/komainu8/postgresql-conference-japan-2021/)

  * [リポジトリー](https://github.com/komainu8/rabbit-slide-komainu8-postgresql-conference-japan-2021)

### 内容

PostgreSQL で使用できる全文検索の拡張に [PGroonga(ぴーじーるんが)](https://pgroonga.github.io/ja/) という高速で高性能な拡張があります。

PGroongaはバックエンドに本格的な全文検索エンジンGroongaを使っており、高速な全文検索以外にも、より良い検索結果を出すための機能が盛り込まれていますが、多機能であるため、どういった機能が存在するのか、それらをどう使うのかは余り知られていない印象です。

本講演では、PGroongaで全文検索結果をより良くする機能について、網羅的に紹介します。
以下の機能を紹介する予定です。

* ノーマライザー
* トークナイザー
* ステミング
* fuzzy検索
* 同義語展開
* スコアー関数のカスタマイズ

### まとめ

この講演は、PGroongaを既に使っている人向けの内容になっています。
PGroongaの便利な機能を使うことに興味がある方は、是非、発表資料を確認してみてください。
