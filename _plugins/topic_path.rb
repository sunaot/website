module Jekyll
  class TopicPath < Liquid::Tag
    include Jekyll::Filters::URLFilters

    def initialize(tag_name, input, tokens)
      super
      @input = input
    end

    def render(context)
      @context = context
      page_url = normalize_url(context["page"]["url"])
      path_components = page_url.split("/")[0..-2]
      all_pages = context["site"]["posts"] + context["site"]["pages"]
      current_page = all_pages.find do |page|
        normalize_url(page.url) == page_url
      end
      ancestors = []
      ancestors << current_page if current_page
      until path_components.empty?
        target = all_pages.find do |page|
          sub_path = path_components.join("/") + "/"
          normalize_url(page.url) == sub_path
        end
        ancestors << target if target
        path_components.pop
      end
      ancestors.reverse_each.map do |page|
        if page == current_page
          content = page.data["title"] || page.data["main_title"]
        else
          content = link_to_page(page)
        end
        topic_path_component(content)
      end.join(" &gt; ")
    end

    def normalize_url(url)
      url.gsub(/\/index\.html\z/, "/")
    end

    def link_to_page(page)
      url = relative_url(normalize_url(page.url))
      escaped_title = page.data["title"] || page.data["main_title"]
      %Q!<a href="#{url}" title="#{escaped_title}">#{escaped_title}</a>!
    end

    def topic_path_component(content)
      %Q!<span class="topic-path-component">#{content}</span>!
    end
  end
end

Liquid::Template.register_tag("topic_path", Jekyll::TopicPath)
